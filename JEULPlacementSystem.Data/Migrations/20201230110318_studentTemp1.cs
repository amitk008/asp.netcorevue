﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JEULPlacementSystem.Data.Migrations
{
    public partial class studentTemp1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StudentTempRegistrations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Email = table.Column<string>(maxLength: 50, nullable: false),
                    CompanyId = table.Column<Guid>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    TenantId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentTempRegistrations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StudentTempRegistrations_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_StudentTempRegistrations_CompanyId",
                table: "StudentTempRegistrations",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentTempRegistrations_Email",
                table: "StudentTempRegistrations",
                column: "Email",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StudentTempRegistrations");
        }
    }
}
