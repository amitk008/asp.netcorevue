﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JEULPlacementSystem.Data.Migrations
{
    public partial class studentTempFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StudentTempRegistrations_Company_CompanyId",
                table: "StudentTempRegistrations");

            migrationBuilder.DropIndex(
                name: "IX_StudentTempRegistrations_CompanyId",
                table: "StudentTempRegistrations");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "StudentTempRegistrations");

            migrationBuilder.AddColumn<Guid>(
                name: "UniversityId",
                table: "StudentTempRegistrations",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_StudentTempRegistrations_UniversityId",
                table: "StudentTempRegistrations",
                column: "UniversityId");

            migrationBuilder.AddForeignKey(
                name: "FK_StudentTempRegistrations_Universities_UniversityId",
                table: "StudentTempRegistrations",
                column: "UniversityId",
                principalTable: "Universities",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StudentTempRegistrations_Universities_UniversityId",
                table: "StudentTempRegistrations");

            migrationBuilder.DropIndex(
                name: "IX_StudentTempRegistrations_UniversityId",
                table: "StudentTempRegistrations");

            migrationBuilder.DropColumn(
                name: "UniversityId",
                table: "StudentTempRegistrations");

            migrationBuilder.AddColumn<Guid>(
                name: "CompanyId",
                table: "StudentTempRegistrations",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_StudentTempRegistrations_CompanyId",
                table: "StudentTempRegistrations",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_StudentTempRegistrations_Company_CompanyId",
                table: "StudentTempRegistrations",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id");
        }
    }
}
