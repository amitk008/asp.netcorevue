﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JEULPlacementSystem.Data.Migrations
{
    public partial class sortlist : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SortListed",
                table: "JobSortListed");

            migrationBuilder.AddColumn<bool>(
                name: "IsEmailSent",
                table: "JobSortListed",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "SortListedDate",
                table: "JobSortListed",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsEmailSent",
                table: "JobSortListed");

            migrationBuilder.DropColumn(
                name: "SortListedDate",
                table: "JobSortListed");

            migrationBuilder.AddColumn<DateTime>(
                name: "SortListed",
                table: "JobSortListed",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
