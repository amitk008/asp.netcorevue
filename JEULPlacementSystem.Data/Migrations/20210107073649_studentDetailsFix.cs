﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JEULPlacementSystem.Data.Migrations
{
    public partial class studentDetailsFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentExperienceDetail",
                table: "StudentExperienceDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentEducationDetail",
                table: "StudentEducationDetail");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "StudentExperienceDetail",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "StudentEducationDetail",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentExperienceDetail",
                table: "StudentExperienceDetail",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentEducationDetail",
                table: "StudentEducationDetail",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentExperienceDetail",
                table: "StudentExperienceDetail");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StudentEducationDetail",
                table: "StudentEducationDetail");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "StudentExperienceDetail");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "StudentEducationDetail");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentExperienceDetail",
                table: "StudentExperienceDetail",
                column: "StudentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StudentEducationDetail",
                table: "StudentEducationDetail",
                column: "StudentId");
        }
    }
}
