﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JEULPlacementSystem.Data.Migrations
{
    public partial class emailcontent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobViewCount_Jobs_JobId",
                table: "JobViewCount");

            migrationBuilder.DropForeignKey(
                name: "FK_JobViewCount_Student_StudentId",
                table: "JobViewCount");

            migrationBuilder.DropPrimaryKey(
                name: "PK_JobViewCount",
                table: "JobViewCount");

            migrationBuilder.RenameTable(
                name: "JobViewCount",
                newName: "JobViewCounts");

            migrationBuilder.RenameIndex(
                name: "IX_JobViewCount_StudentId",
                table: "JobViewCounts",
                newName: "IX_JobViewCounts_StudentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_JobViewCounts",
                table: "JobViewCounts",
                columns: new[] { "JobId", "StudentId" });

            migrationBuilder.CreateTable(
                name: "EmailContents",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedById = table.Column<Guid>(nullable: true),
                    ModifiedById = table.Column<Guid>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsApproved = table.Column<bool>(nullable: false),
                    ApprovedById = table.Column<Guid>(nullable: true),
                    ApprovedDate = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    CompanyId = table.Column<Guid>(nullable: false),
                    SelectedEmailContent = table.Column<string>(nullable: true),
                    NotSelectedEmailContent = table.Column<string>(nullable: true),
                    TenantId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailContents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmailContents_AspNetUsers_ApprovedById",
                        column: x => x.ApprovedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmailContents_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_EmailContents_AspNetUsers_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmailContents_AspNetUsers_ModifiedById",
                        column: x => x.ModifiedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmailContents_ApprovedById",
                table: "EmailContents",
                column: "ApprovedById");

            migrationBuilder.CreateIndex(
                name: "IX_EmailContents_CompanyId",
                table: "EmailContents",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_EmailContents_CreatedById",
                table: "EmailContents",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_EmailContents_ModifiedById",
                table: "EmailContents",
                column: "ModifiedById");

            migrationBuilder.AddForeignKey(
                name: "FK_JobViewCounts_Jobs_JobId",
                table: "JobViewCounts",
                column: "JobId",
                principalTable: "Jobs",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_JobViewCounts_Student_StudentId",
                table: "JobViewCounts",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobViewCounts_Jobs_JobId",
                table: "JobViewCounts");

            migrationBuilder.DropForeignKey(
                name: "FK_JobViewCounts_Student_StudentId",
                table: "JobViewCounts");

            migrationBuilder.DropTable(
                name: "EmailContents");

            migrationBuilder.DropPrimaryKey(
                name: "PK_JobViewCounts",
                table: "JobViewCounts");

            migrationBuilder.RenameTable(
                name: "JobViewCounts",
                newName: "JobViewCount");

            migrationBuilder.RenameIndex(
                name: "IX_JobViewCounts_StudentId",
                table: "JobViewCount",
                newName: "IX_JobViewCount_StudentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_JobViewCount",
                table: "JobViewCount",
                columns: new[] { "JobId", "StudentId" });

            migrationBuilder.AddForeignKey(
                name: "FK_JobViewCount_Jobs_JobId",
                table: "JobViewCount",
                column: "JobId",
                principalTable: "Jobs",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_JobViewCount_Student_StudentId",
                table: "JobViewCount",
                column: "StudentId",
                principalTable: "Student",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
