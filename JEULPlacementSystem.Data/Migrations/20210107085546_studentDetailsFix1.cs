﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace JEULPlacementSystem.Data.Migrations
{
    public partial class studentDetailsFix1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_StudentExperienceDetail_StudentId",
                table: "StudentExperienceDetail");

            migrationBuilder.DropIndex(
                name: "IX_StudentEducationDetail_StudentId",
                table: "StudentEducationDetail");

            migrationBuilder.CreateIndex(
                name: "IX_StudentExperienceDetail_StudentId",
                table: "StudentExperienceDetail",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentEducationDetail_StudentId",
                table: "StudentEducationDetail",
                column: "StudentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_StudentExperienceDetail_StudentId",
                table: "StudentExperienceDetail");

            migrationBuilder.DropIndex(
                name: "IX_StudentEducationDetail_StudentId",
                table: "StudentEducationDetail");

            migrationBuilder.CreateIndex(
                name: "IX_StudentExperienceDetail_StudentId",
                table: "StudentExperienceDetail",
                column: "StudentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_StudentEducationDetail_StudentId",
                table: "StudentEducationDetail",
                column: "StudentId",
                unique: true);
        }
    }
}
