﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JEULPlacementSystem.Data.Migrations
{
    public partial class studentDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Student_UserId",
                table: "Student");

            migrationBuilder.AddColumn<int>(
                name: "StudentProfileStudentId",
                table: "Student",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "StudentEducationDetail",
                columns: table => new
                {
                    StudentId = table.Column<int>(nullable: false),
                    Degree = table.Column<string>(nullable: true),
                    Faculty = table.Column<string>(nullable: true),
                    Institute = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    GradingTypeId = table.Column<int>(nullable: false),
                    Marks = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentEducationDetail", x => x.StudentId);
                    table.ForeignKey(
                        name: "FK_StudentEducationDetail_Student_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Student",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StudentExperienceDetail",
                columns: table => new
                {
                    StudentId = table.Column<int>(nullable: false),
                    CompanyName = table.Column<string>(nullable: true),
                    JobTitle = table.Column<string>(nullable: true),
                    JobTypeId = table.Column<int>(nullable: false),
                    IsCurrentJob = table.Column<bool>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Responsibilities = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentExperienceDetail", x => x.StudentId);
                    table.ForeignKey(
                        name: "FK_StudentExperienceDetail_Student_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Student",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StudentProfile",
                columns: table => new
                {
                    StudentId = table.Column<int>(nullable: false),
                    Gender = table.Column<string>(nullable: true),
                    MobileNumber = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: true),
                    MartialStatusId = table.Column<int>(nullable: false),
                    Nationality = table.Column<string>(nullable: true),
                    JobTypeId = table.Column<int>(nullable: false),
                    CurrentSalary = table.Column<string>(nullable: true),
                    ExpectSalary = table.Column<string>(nullable: true),
                    Skills = table.Column<string>(nullable: true),
                    Objective = table.Column<string>(nullable: true),
                    PermanentAddress = table.Column<string>(nullable: true),
                    CurrentAddress = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentProfile", x => x.StudentId);
                    table.ForeignKey(
                        name: "FK_StudentProfile_Student_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Student",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Student_StudentProfileStudentId",
                table: "Student",
                column: "StudentProfileStudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Student_UserId",
                table: "Student",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_StudentEducationDetail_StudentId",
                table: "StudentEducationDetail",
                column: "StudentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_StudentExperienceDetail_StudentId",
                table: "StudentExperienceDetail",
                column: "StudentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_StudentProfile_StudentId",
                table: "StudentProfile",
                column: "StudentId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Student_StudentProfile_StudentProfileStudentId",
                table: "Student",
                column: "StudentProfileStudentId",
                principalTable: "StudentProfile",
                principalColumn: "StudentId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Student_StudentProfile_StudentProfileStudentId",
                table: "Student");

            migrationBuilder.DropTable(
                name: "StudentEducationDetail");

            migrationBuilder.DropTable(
                name: "StudentExperienceDetail");

            migrationBuilder.DropTable(
                name: "StudentProfile");

            migrationBuilder.DropIndex(
                name: "IX_Student_StudentProfileStudentId",
                table: "Student");

            migrationBuilder.DropIndex(
                name: "IX_Student_UserId",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "StudentProfileStudentId",
                table: "Student");

            migrationBuilder.CreateIndex(
                name: "IX_Student_UserId",
                table: "Student",
                column: "UserId");
        }
    }
}
