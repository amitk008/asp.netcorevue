﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JEULPlacementSystem.Data.Migrations
{
    public partial class jobfix12111 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "JobApplied",
                columns: table => new
                {
                    JobId = table.Column<Guid>(nullable: false),
                    StudentId = table.Column<int>(nullable: false),
                    AppliedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobApplied", x => new { x.JobId, x.StudentId });
                    table.ForeignKey(
                        name: "FK_JobApplied_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_JobApplied_Student_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Student",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "JobSaved",
                columns: table => new
                {
                    JobId = table.Column<Guid>(nullable: false),
                    StudentId = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobSaved", x => new { x.JobId, x.StudentId });
                    table.ForeignKey(
                        name: "FK_JobSaved_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_JobSaved_Student_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Student",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JobViewCount",
                columns: table => new
                {
                    JobId = table.Column<Guid>(nullable: false),
                    StudentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobViewCount", x => new { x.JobId, x.StudentId });
                    table.ForeignKey(
                        name: "FK_JobViewCount_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_JobViewCount_Student_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Student",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobApplied_StudentId",
                table: "JobApplied",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_JobSaved_StudentId",
                table: "JobSaved",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_JobViewCount_StudentId",
                table: "JobViewCount",
                column: "StudentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobApplied");

            migrationBuilder.DropTable(
                name: "JobSaved");

            migrationBuilder.DropTable(
                name: "JobViewCount");
        }
    }
}
