﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JEULPlacementSystem.Data.Migrations
{
    public partial class jobAddProp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EligibilityMajor",
                table: "Job");

            migrationBuilder.AddColumn<Guid>(
                name: "DegreeId",
                table: "Job",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "JobDescription",
                table: "Job",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NoOfVacancy",
                table: "Job",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Job_DegreeId",
                table: "Job",
                column: "DegreeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Job_Degrees_DegreeId",
                table: "Job",
                column: "DegreeId",
                principalTable: "Degrees",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Job_Degrees_DegreeId",
                table: "Job");

            migrationBuilder.DropIndex(
                name: "IX_Job_DegreeId",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "DegreeId",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "JobDescription",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "NoOfVacancy",
                table: "Job");

            migrationBuilder.AddColumn<string>(
                name: "EligibilityMajor",
                table: "Job",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
