﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JEULPlacementSystem.Data.Migrations
{
    public partial class jobsortlist : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "JobSortListed",
                columns: table => new
                {
                    JobId = table.Column<Guid>(nullable: false),
                    StudentId = table.Column<int>(nullable: false),
                    IsSortListed = table.Column<bool>(nullable: false),
                    SortListed = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobSortListed", x => new { x.JobId, x.StudentId });
                    table.ForeignKey(
                        name: "FK_JobSortListed_Jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "Jobs",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_JobSortListed_Student_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Student",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_JobSortListed_StudentId",
                table: "JobSortListed",
                column: "StudentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobSortListed");
        }
    }
}
