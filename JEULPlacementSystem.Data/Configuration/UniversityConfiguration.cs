﻿using JEULPlacementSystem.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JEULPlacementSystem.Data.Configuration
{
    public class UniversityConfiguration : IEntityTypeConfiguration<University>
    {
        public void Configure(EntityTypeBuilder<University> builder)
        {
            builder.HasKey(e => e.Id);

            builder.HasOne(e => e.CreatedBy)
                .WithMany()
                .HasForeignKey(e => e.CreatedById);

            builder.HasOne(e => e.ModifiedBy)
                .WithMany()
                .HasForeignKey(e => e.ModifiedById);

        }
    }

    public class UniversityUserConfiguration : IEntityTypeConfiguration<UniversityUser>
    {
        public void Configure(EntityTypeBuilder<UniversityUser> builder)
        {
           

            builder.HasKey(e => new { e.UserId, e.UniversityId });

            builder.HasOne(e => e.University)
                   .WithMany(e => e.UniversityUsers)
                   .HasForeignKey(e => e.UniversityId)
                   .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(e => e.User)
                   .WithMany(e => e.UniversityUsers)
                   .HasForeignKey(e => e.UserId)
                   .OnDelete(DeleteBehavior.NoAction);

        }
    }


}