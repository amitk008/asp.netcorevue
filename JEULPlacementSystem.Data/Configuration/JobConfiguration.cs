﻿using JEULPlacementSystem.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JEULPlacementSystem.Data.Configuration
{
    public class JobConfiguration : IEntityTypeConfiguration<Job>
    {
        public void Configure(EntityTypeBuilder<Job> builder)
        {
            builder.HasKey(e => e.Id);

            builder.HasOne(e => e.CreatedBy)
                .WithMany()
                .HasForeignKey(e => e.CreatedById);

            builder.HasOne(e => e.ModifiedBy)
                .WithMany()
                .HasForeignKey(e => e.ModifiedById);

            builder.HasOne(e => e.Company)
                 .WithMany()
                 .HasForeignKey(e => e.CompanyId)
                 .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(e => e.Degree)
                .WithMany()
                .HasForeignKey(e => e.DegreeId)
                .OnDelete(DeleteBehavior.NoAction);


        }
    }

    public class JobViewCountConfiguration : IEntityTypeConfiguration<JobViewCount>
    {
        public void Configure(EntityTypeBuilder<JobViewCount> builder)
        {
            builder.HasKey(e => new { e.JobId, e.StudentId });


            builder.HasOne(e => e.Job)
               .WithMany(d => d.JobViewCounts)
               .HasForeignKey(e => e.JobId)
               .OnDelete(DeleteBehavior.NoAction);

        }
    }



    public class JobSavedCountConfiguration : IEntityTypeConfiguration<JobSaved>
    {
        public void Configure(EntityTypeBuilder<JobSaved> builder)
        {
            builder.HasKey(e => new { e.JobId, e.StudentId });


            builder.HasOne(e => e.Job)
               .WithMany()
               .HasForeignKey(e => e.JobId)
               .OnDelete(DeleteBehavior.NoAction);

        }
    }
    public class JobAppliedConfiguration : IEntityTypeConfiguration<JobApplied>
    {
        public void Configure(EntityTypeBuilder<JobApplied> builder)
        {
            builder.HasKey(e => new { e.JobId, e.StudentId });


            builder.HasOne(e => e.Job)
               .WithMany(d => d.JobsApplied)
               .HasForeignKey(e => e.JobId)
               .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(e => e.Student)
         .WithMany(d => d.JobApplieds)
         .HasForeignKey(e => e.StudentId)
         .OnDelete(DeleteBehavior.NoAction);

        }
    }

    public class JobSortlistConfiguration : IEntityTypeConfiguration<JobSortListed>
    {
        public void Configure(EntityTypeBuilder<JobSortListed> builder)
        {
            builder.HasKey(e => new { e.JobId, e.StudentId });


            builder.HasOne(e => e.Job)
               .WithMany(d => d.JobSortLists)
               .HasForeignKey(e => e.JobId)
               .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(e => e.Student)
                 .WithMany(d => d.JobSortLists)
                 .HasForeignKey(e => e.StudentId)
                 .OnDelete(DeleteBehavior.NoAction);

        }
    }

}