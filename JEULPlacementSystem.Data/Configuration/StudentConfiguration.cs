﻿using JEULPlacementSystem.Domain; 
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JEULPlacementSystem.Data.Configuration
{
    public class StudentTempConfiguration : IEntityTypeConfiguration<StudentTempRegistration>
    {
        public void Configure(EntityTypeBuilder<StudentTempRegistration> builder)
        {
            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.Email).IsUnique();
            
            builder.Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(50);

            builder.HasOne(e => e.University)
               .WithMany()
               .HasForeignKey(e => e.UniversityId)
               .OnDelete(DeleteBehavior.NoAction);

        }
    }


    public class StudentConfiguration : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.UserId).IsUnique();

            builder.HasOne(e => e.User)
               .WithMany()
               .HasForeignKey(e => e.UserId)
               .OnDelete(DeleteBehavior.NoAction);


            builder.HasOne(e => e.University)
               .WithMany()
               .HasForeignKey(e => e.UniversityId)
               .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(d => d.User)
               .WithMany(p => p.Students)
               .HasForeignKey(d => d.UserId)
               .OnDelete(DeleteBehavior.ClientSetNull);


            builder.HasOne(s => s.CreatedBy)
                .WithMany()
                .HasForeignKey(s => s.CreatedById);

       

        }
    }


    public class StudentProfileConfiguration : IEntityTypeConfiguration<StudentProfile>
    {
        public void Configure(EntityTypeBuilder<StudentProfile> builder)
        {
            builder.HasKey(e => e.StudentId);

            builder.HasIndex(e => e.StudentId).IsUnique();

            builder.HasOne(e => e.Student)
               .WithMany()
               .HasForeignKey(e => e.StudentId)
               .OnDelete(DeleteBehavior.NoAction);

             

        }
    }

    public class StudentEducationDetailConfiguration : IEntityTypeConfiguration<StudentEducationDetail>
    {
        public void Configure(EntityTypeBuilder<StudentEducationDetail> builder)
        {
            builder.HasKey(e => e.Id); 

            builder.HasOne(e => e.Student)
               .WithMany()
               .HasForeignKey(e => e.StudentId)
               .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(d => d.Student)
               .WithMany(p => p.StudentEducationDetails)
               .HasForeignKey(d => d.StudentId)
               .OnDelete(DeleteBehavior.ClientSetNull);

        }
    }

    public class StudentExperienceDetailConfiguration : IEntityTypeConfiguration<StudentExperienceDetail>
    {
        public void Configure(EntityTypeBuilder<StudentExperienceDetail> builder)
        {
            builder.HasKey(e => e.Id);
             

            builder.HasOne(e => e.Student)
               .WithMany()
               .HasForeignKey(e => e.StudentId)
               .OnDelete(DeleteBehavior.NoAction);


            builder.HasOne(d => d.Student)
               .WithMany(p => p.StudentExperienceDetails)
               .HasForeignKey(d => d.StudentId)
               .OnDelete(DeleteBehavior.ClientSetNull);

        }
    }
}