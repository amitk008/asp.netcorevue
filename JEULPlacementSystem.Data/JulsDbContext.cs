﻿using System;
using JEULPlacementSystem.Domain; 
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace JEULPlacementSystem.Data
{
    public class JulsDbContext : IdentityDbContext<User, Role, Guid, IdentityUserClaim<Guid>, UserRole,
        IdentityUserLogin<Guid>, IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>
    {


        public JulsDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }
        public DbSet<University> Universities { get; set; }

        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<Degree> Degrees { get; set; }
        public DbSet<Placement> Placements { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<UserPage> UserPages { get; set; }
        public DbSet<StudentTempRegistration> StudentTempRegistrations { get; set; }
        public DbSet<Job> Jobs { get; set; }

        public DbSet<JobApplied> JobApplied { get; set; }
        public DbSet<JobSaved> JobSaved { get; set; }
        public DbSet<JobViewCount> JobViewCounts { get; set; }

        public DbSet<JobSortListed> JobSortListed { get; set; }
        public DbSet<EmailContent> EmailContents { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);

            modelBuilder.Entity<StudentExperienceDetail>().HasQueryFilter(x => x.IsActive );

            modelBuilder.Entity<StudentEducationDetail>().HasQueryFilter(x => x.IsActive);
        }

    }
}
