﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace JEULPlacementSystem.Data
{
    public class JEULDbContextFactory : DesignTimeDbContextFactoryBase<JulsDbContext>
    {
        

        protected override JulsDbContext CreateNewInstance(DbContextOptions<JulsDbContext> options)
        {
            return new JulsDbContext(options);
        }

        protected override JulsDbContext CreateNewInstance(DbContextOptions<JulsDbContext> options, IConfiguration configuration)
        {
            return new JulsDbContext(options);
        }
    }
}