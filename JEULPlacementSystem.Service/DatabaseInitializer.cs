﻿using JEULPlacementSystem.Data;
using JEULPlacementSystem.Data.Extensions;
using JEULPlacementSystem.Domain;
using JEULPlacementSystem.Domain.Constant;
using JEULPlacementSystem.Domain.Enum;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service
{
    public interface IDatabaseInitializer
    {
        Task SeedAsync();
    }

    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly JulsDbContext _context;
        private readonly ILogger _logger;
        private readonly RoleManager<Role> _roleManager;
        private readonly UserManager<User> _userManager;

        public DatabaseInitializer(JulsDbContext context, ILogger<DatabaseInitializer> logger,
            UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            _context = context;
            _logger = logger;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public virtual async Task SeedAsync()
        {
            try
            {
                await _context.Database.MigrateAsync().ConfigureAwait(false);

                await SeedUserManagement(_context);

            }

            catch (Exception ex)
            {

            }
        }

        private async Task<User> CreateUserAsync(User user, string password, string role)
        {
            if (await _userManager.FindByNameAsync(user.UserName) == null)
            {
                var result = await _userManager.CreateAsync(user, password);

                if (result.Succeeded)
                {
                    user = await _userManager.FindByNameAsync(user.UserName);
                    result = await _userManager.AddToRoleAsync(user, role);

                    if (result.Succeeded)
                        return user;
                    await _userManager.DeleteAsync(user);
                }

                throw new Exception(
                    $"Seeding \"{user.UserName}\" user failed. Errors: {string.Join(Environment.NewLine, result.Errors)}");
            }

            return user;
        }


        private async Task EnsureRoleAsync(Role role, List<int> menuIds)
        {
            _context.Roles.AddOrUpdate(ref role, x => new { x.Id });

            var userPages = new List<UserPage>();

            if (menuIds.Any())

                menuIds.Distinct().ToList()
                    .ForEach(m => { userPages.Add(new UserPage { PageId = m, RoleId = role.Id }); });

            userPages.ForEach(c => { _context.UserPages.AddOrUpdate(ref c, u => new { u.PageId, u.RoleId }); });


            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

        #region User Management

        public async Task SeedUserManagement(JulsDbContext context)
        {
            #region Pages


            var pages = new List<Page>
                {
                    new Page { Id = 1,  Path = "/dashboard",               Title = "Dashboard",              DisplayOrder = 1, Icon = "fa fa-university", ShowInView = true   },
                    new Page { Id = 2,  Path = "/university",              Title = "University",             DisplayOrder = 2, Icon = "fa fa-university", ShowInView = true   },
                    new Page { Id = 3,  Path = "/faculty",                 Title = "Faculty",                DisplayOrder = 3, Icon = "fa fa-university", ShowInView = true   },
                    new Page { Id = 4,  Path = "/degree",                  Title = "Degree",                 DisplayOrder = 4, Icon = "fa fa-university", ShowInView = true   },
                    new Page { Id = 5,  Path = "/company",                 Title = "Company",                DisplayOrder = 5, Icon = "fa fa-university", ShowInView = true   },
                    new Page { Id = 6,  Path = "/placement",               Title = "Placement Info",         DisplayOrder = 6, Icon = "fa fa-university", ShowInView = true   },
                    new Page { Id = 7,  Path = "/job",                     Title = "Job",                    DisplayOrder = 7, Icon = "fa fa-tasks",      ShowInView = true   },
                    new Page { Id = 8,  Path = "/student",                 Title = "Student",                DisplayOrder = 8, Icon = "fa fa-users ",     ShowInView = true   },
                    new Page { Id = 9,  Path = "/student/list",            Title = "Student List",           DisplayOrder = 1, Icon = "fa fa-users",      ShowInView = true , ParentPageId = 8,  },
                    new Page { Id = 10, Path = "/student/onboarding",      Title = "Student Onboarding",     DisplayOrder = 2, Icon = "fa fa-user-plus",  ShowInView = true , ParentPageId = 8,  },

                    new Page { Id = 11,  Path = "/setting",                Title = "Setting",                DisplayOrder = 9, Icon = "fa fa-users ",     ShowInView = true   },
                    new Page { Id = 12,  Path = "/setting/emailtemplate",  Title = "Email Template",         DisplayOrder = 1, Icon = "fa fa-users",      ShowInView = true , ParentPageId = 11,  },


                };

            pages.ForEach(c => { context.Pages.AddOrUpdate(ref c, x => new { x.Id }); });
            context.SaveChanges();




            #endregion

            var superAdminMenu = new List<int> { 1, 2, };

            var universityMenu = new List<int> { 1, 3, 4, 5, 6, 8, 9, 10 };

            var companyMenu = new List<int> { 1, 7,11,12 };

            var studentMenu = new List<int> { 1 };

            var superAdminRole = new Role
            {
                Id = new Guid(ApplicationConstants.SuperAdminRoleGuid),
                Name = "Super Admin",
                NormalizedName = "SUPER ADMIN",
                Description = "Super Admin User",
                IsActive = true,
                SystemDefined = true,
                Hierarchy = 1
            };
            await EnsureRoleAsync(superAdminRole, superAdminMenu);

            var universityRole = new Role
            {
                Id = new Guid(ApplicationConstants.UniversityRoleGuid),
                Name = "University User",
                NormalizedName = "UNIVERSITY USER",
                Description = "University User",
                IsActive = true,
                SystemDefined = true,
                Hierarchy = 2
            };
            await EnsureRoleAsync(universityRole, universityMenu);

            var companyMenuRole = new Role
            {
                Id = new Guid(ApplicationConstants.CompanyRoleGuid),
                Name = "Company User",
                NormalizedName = "COMPANY USER",
                Description = "Company User",
                IsActive = true,
                SystemDefined = true,
                Hierarchy = 3
            };
            await EnsureRoleAsync(companyMenuRole, companyMenu);

            var superadminemail = "admin@jeulsystem.co";
            var superUser = new User
            {
                Id = new Guid(ApplicationConstants.SuperAdminUserGuid),
                FirstName = "Super",
                LastName = "Admin",
                FullName = "Super Admin",
                EmailConfirmed = true,
                Email = superadminemail,
                UserType = UserType.SuperAdmin,
                NormalizedEmail = superadminemail.ToUpper(),
                UserName = superadminemail,
                NormalizedUserName = superadminemail.ToUpper(),
                SecurityStamp = Guid.NewGuid().ToString(),
                CreatedDate = DateTime.Now,
                SystemDefined = true,
                IsActive = true,

            };
            await CreateUserAsync(superUser, "Admin@123", superAdminRole.Name);


            var studentMenuRole = new Role
            {
                Id = new Guid(ApplicationConstants.StudentRoleGuid),
                Name = "Student",
                NormalizedName = "STUDENT",
                Description = "STUDENT User",
                IsActive = true,
                SystemDefined = true,
                Hierarchy = 3
            };
            await EnsureRoleAsync(studentMenuRole, studentMenu);

            context.SaveChanges();


        }
        #endregion

    }
}