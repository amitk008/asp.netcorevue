﻿using JEULPlacementSystem.Common.ViewModel;
using Kendo.DynamicLinqCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service.Interface
{
    public interface IFacultyService
    {
        Task<FacultyViewModel> GetAsync(Guid id,Guid TenantId);
        DataSourceResult GetAll(KendoDataRequest kendoDataRequest, Guid tenantId);
        Task CreateAsync(FacultyViewModel faculty, Guid tenantId);
        Task UpdateAsync(FacultyViewModel faculty, Guid tenantId);
        Task DeleteAsync(Guid id, Guid tenantId);
        IList<ListViewModel> ListFaculty(Guid tenantId);
    }
}
