﻿using JEULPlacementSystem.Common.ViewModel;
using Kendo.DynamicLinqCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service.Interface
{
    public interface IPlacementService
    {
        Task<PlacementViewModel> GetAsync(Guid id,Guid TenantId);
        DataSourceResult GetAll(KendoDataRequest kendoDataRequest, Guid tenantId);
        Task CreateAsync(PlacementViewModel Placement, Guid tenantId);
        Task UpdateAsync(PlacementViewModel Placement, Guid tenantId);
        Task DeleteAsync(Guid id, Guid tenantId);
    }
}
