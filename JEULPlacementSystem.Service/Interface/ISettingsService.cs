﻿using JEULPlacementSystem.Common.ViewModel;
using System;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service.Interface
{
    public interface ISettingsService
    {
  
        Task AddEditEmailTemplate(EmailTemplateViewModel model, Guid tenantId);
        Task<EmailTemplateViewModel> GetEmailTemplateAsync(Guid tenantId);
    }
}
