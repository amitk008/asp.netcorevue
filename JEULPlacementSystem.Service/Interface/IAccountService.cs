﻿using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Model.Common;
using System;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service.Interface
{
    public interface IAccountService
    {
        Task<Response> Login(SignInViewModel model);
        Task<Response> ResetPassword(ResetPasswordViewModel model);
        Task<Response> ForgotPassword(ForgotPasswordViewModel model); 
    }
}
