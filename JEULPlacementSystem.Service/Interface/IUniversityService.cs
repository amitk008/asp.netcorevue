﻿using JEULPlacementSystem.Common.ViewModel;
using Kendo.DynamicLinqCore;
using System;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service.Interface
{
    public interface IUniversityService
    {
        Task<UniversityViewModel> GetAsync(Guid id);
        DataSourceResult GetAll(KendoDataRequest kendoDataRequest);
        Task CreateAsync(UniversityViewModel university);
        Task UpdateAsync(UniversityViewModel university);
        Task DeleteAsync(Guid id);
        Task<UniversityViewModel> Approve(Guid id);
        Task<UniversityViewModel> ActivateDeactivateUniversity(Guid id);
    }
}
