﻿using JEULPlacementSystem.Common.ViewModel;
using Kendo.DynamicLinqCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service.Interface
{
    public interface IStudentService
    {
     
        Task<DataSourceResult> OnBoardingRegistration(List<StudentOnBoardingViewModel> model, Guid tenantId);

        Task Registration(StudentViewModel model);
        DataSourceResult GetAll(KendoDataRequest kendoDataRequest, Guid tenantId);
        Task<StudentViewModel> GetAsync(int id, Guid tenantId);

        Task<List<StudentJobListViewModel>> GetAllJobList(string title , Guid tenantId);
        Task ApplyJob(JobCounterViewModel model);
        Task UpdateViewCountAsync(JobCounterViewModel model);
        Task<bool> SaveJobAsync(JobCounterViewModel model);
        Task AddUpdateProfileAsync(MyProfileViewModel model, Guid tenantId);
        Task<MyProfileViewModel> GetProfileAsync(Guid currentUserId, Guid tenantId);
        Task<MyProfileViewModel> GetDetails(int id, Guid tenantId);
        Task<Tuple<MyProfileViewModel, string>> DownloadResume(int id, Guid tenantId);
    }
}
