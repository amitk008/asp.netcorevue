﻿using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Model.Common;
using System;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service.Interface
{
    public interface IProfileService
    {
        Task<Response> UpdateProfile(MyProfileViewModel model, Guid tenantId);
        Task<MyProfileViewModel> GetAsync(Guid tenantId);
    }
}
