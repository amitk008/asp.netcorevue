﻿using JEULPlacementSystem.Common.ViewModel;
using Kendo.DynamicLinqCore;
using System;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service.Interface
{
    public interface ICompanyService
    {
        Task<CompanyViewModel> GetAsync(Guid id, Guid tenantId);
        DataSourceResult GetAll(KendoDataRequest kendoDataRequest, Guid tenantId);
        Task CreateAsync(CompanyViewModel company, Guid tenantId);
        Task UpdateAsync(CompanyViewModel company, Guid tenantId);
        Task DeleteAsync(Guid id, Guid tenantId);
    }
}
