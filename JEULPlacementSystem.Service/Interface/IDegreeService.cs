﻿using JEULPlacementSystem.Common.ViewModel;
using Kendo.DynamicLinqCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service.Interface
{
    public interface IDegreeService
    {
        Task<DegreeViewModel> GetAsync(Guid id, Guid tenantId);
        DataSourceResult GetAll(KendoDataRequest kendoDataRequest, Guid tenantId);
        Task CreateAsync(DegreeViewModel degree, Guid tenantId);
        Task UpdateAsync(DegreeViewModel degree, Guid tenantId);
        Task DeleteAsync(Guid id, Guid tenantId);
        IList<ListViewModel> ListDegrees(Guid tenantId);
    }
}
