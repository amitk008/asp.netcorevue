﻿using JEULPlacementSystem.Common.ViewModel;
using Kendo.DynamicLinqCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service.Interface
{
    public interface IJobService
    {
        Task<JobViewModel> GetAsync(Guid id);
        DataSourceResult GetAll(KendoDataRequest kendoDataRequest, Guid? tenantId);
        Task CreateAsync(JobViewModel Job, Guid tenantId);
        Task UpdateAsync(JobViewModel Job, Guid tenantId);
        Task DeleteAsync(Guid id, Guid tenantId);
        List<StudentListViewModel> GetStudentListByJob(Guid id, string name, Guid tenantId);
        Task SortListStudents(Guid id, JobSortListViewModel model, Guid tenantId);
    }
}
