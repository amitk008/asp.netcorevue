﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service.Interface
{
    public interface IEmailService
    {
        Task SendEmailAsync(string email, string templateId, dynamic message);
    }
}
