﻿using JEULPlacementSystem.Common;
using System.Security.Claims;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service.Interface
{
    public interface IJwtFactory
    {
        Task<string> GenerateEncodedToken(string userName, string role, ClaimsIdentity identity);

        ClaimsIdentity GenerateClaimsIdentity(string userName, LoginInfo loginInfo);
    }
}