﻿using DinkToPdf;
using DinkToPdf.Contracts;

namespace JEULPlacementSystem.Service.Infrastructure
{
    public class PDFBuilder
    {
        public static byte[] ConvertHtmltoPdf(IConverter converter, string html, bool displayPagination = false)
        {
            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 40 },
            };


            var objectSettings = new ObjectSettings
            {
                HtmlContent = html,
                WebSettings = { DefaultEncoding = "utf-8", },
            };
            if (displayPagination)
            {
                objectSettings.PagesCount = true;
                objectSettings.FooterSettings = new FooterSettings
                { FontName = "Verdana", FontSize = 8, Line = true, Right = "Page [page] of [toPage]" };
            }

            var pdf = new HtmlToPdfDocument
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };


            return converter.Convert(pdf);
        }
    }
}
