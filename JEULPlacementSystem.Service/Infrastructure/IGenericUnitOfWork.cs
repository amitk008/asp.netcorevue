﻿using System;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service
{
    public interface IGenericUnitOfWork : IDisposable
    {
        GenericRepository<TEntity, TKey> GetRepository<TEntity, TKey>() where TEntity : class;
        Task SaveChangesAsync();
    }
}