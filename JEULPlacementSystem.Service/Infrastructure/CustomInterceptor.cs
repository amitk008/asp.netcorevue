﻿using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using FluentValidation.AspNetCore;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using ValidationException = JEULPlacementSystem.Common.Exceptions.ValidationException;

namespace JEULPlacementSystem.Service.Infrastructure
{
       public class CustomInterceptor : IValidatorInterceptor
    {
        public List<string> Failures { get; }


   

        IValidationContext IValidatorInterceptor.BeforeMvcValidation(ControllerContext controllerContext, IValidationContext commonContext)
        {
            return commonContext;
        }

        ValidationResult IValidatorInterceptor.AfterMvcValidation(ControllerContext controllerContext, IValidationContext commonContext, ValidationResult result)
        {
            var projection = result.Errors.ToList();
            if (projection.Count > 0)
                throw new ValidationException(projection);
            return new ValidationResult(projection);
        }
    }
}
