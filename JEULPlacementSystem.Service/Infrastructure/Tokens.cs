﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using JEULPlacementSystem.Common.Configuration;
using JEULPlacementSystem.Service.Interface;
using Newtonsoft.Json;
using JEULPlacementSystem.Common;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Domain;

namespace JEULPlacementSystem.Service
{
    public class Tokens
    {
        public static async Task<SignInResponseViewModel> GenerateJwt(User user, ClaimsIdentity identity,
            IJwtFactory jwtFactory, JwtIssuerOptions jwtOptions, JsonSerializerSettings serializerSettings)
        {
            var loginInfoSerialized = identity.Claims.Single(c => c.Type == "logininfo").Value;
            var loginInfo = JsonConvert.DeserializeObject<LoginInfo>(loginInfoSerialized);

            try
            {
                var response = new SignInResponseViewModel
                {

                    AccessToken = await jwtFactory.GenerateEncodedToken(user.UserName, loginInfo.Role, identity),
                    ExpiresIn = (int)jwtOptions.ValidFor.TotalSeconds,
                    LoginInfo = loginInfo
                };

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}