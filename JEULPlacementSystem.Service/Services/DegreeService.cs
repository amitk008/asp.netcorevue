﻿using AutoMapper;
using JEULPlacementSystem.Common.Configuration;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Domain;
using JEULPlacementSystem.Service.Interface;
using Kendo.DynamicLinqCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service.Services
{
    public class DegreeService : IDegreeService
    {
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        private readonly IMapper _mapper;

        public DegreeService(IGenericUnitOfWork genericUnitOfWork, IMapper mapper)
        {
            _genericUnitOfWork = genericUnitOfWork;
            _mapper = mapper;
        }

        public async Task CreateAsync(DegreeViewModel model, Guid tenantId)
        {
            var repo = _genericUnitOfWork.GetRepository<Degree, string>();

            var degree = _mapper.Map<DegreeViewModel, Degree>(model);
            degree.Id = Guid.NewGuid();
            degree.IsApproved = true;
            degree.CreatedById = CustomHttpContext.GetUserLoginInfo().UserId;
            degree.CreatedDate = GeneralService.CurrentDate;
            
            degree.TenantId = tenantId;

            repo.Add(degree);

            await _genericUnitOfWork.SaveChangesAsync();

        }

        public async Task DeleteAsync(Guid id, Guid tenantId)
        {

            var degree = await _genericUnitOfWork.GetRepository<Degree, Guid>().FirstOrDefaultAsync(x => x.Id == id && x.TenantId == tenantId);

            if (degree == null)
                throw new ArgumentException("Degree does not exist");
            degree.IsActive = false;
            degree.IsDeleted = true;
            degree.DeletedDate = GeneralService.CurrentDate;

            await _genericUnitOfWork.SaveChangesAsync();

        }

        public DataSourceResult GetAll(KendoDataRequest kendoDataRequest, Guid tenantId)
        {
            var degrees = _genericUnitOfWork.GetRepository<Degree, string>().GetAll(x => x.IsActive && !x.IsDeleted && x.TenantId == tenantId);


            return degrees.ToDataSourceResult<Degree, DegreeListViewModel>
                        (_mapper, kendoDataRequest, new Sort() { Field = "CreatedDate", Dir = "desc" });

        }

        public async Task<DegreeViewModel> GetAsync(Guid id, Guid tenantId)
        {
            var degree = await _genericUnitOfWork.GetRepository<Degree, Guid>().FirstOrDefaultAsync(x => x.Id == id && x.TenantId == tenantId);
            return _mapper.Map<Degree, DegreeViewModel>(degree); ;
        }

        public IList<ListViewModel> ListDegrees(Guid tenantId)
        {
            var repo = _genericUnitOfWork.GetRepository<Degree, Guid>();

            Expression<Func<Degree, bool>> expression = c =>
                c.IsApproved && c.IsActive && !c.IsDeleted && c.TenantId == tenantId ;

            var result =
                _mapper.Map<IList<Degree>, IList<ListViewModel>>(repo.GetAll(expression).OrderBy(o => o.Name)
                    .ToList());
            return result;
        }

        public async Task UpdateAsync(DegreeViewModel model, Guid tenantId)
        {
            var repo = _genericUnitOfWork.GetRepository<Degree, Guid>();
            var degree = await repo.FirstOrDefaultAsync(x => x.Id == model.Id && x.TenantId == tenantId);

            if (degree == null)
                throw new ArgumentException("Degree does not exist");



            var degreeMapped = _mapper.Map(model, degree);

            degreeMapped.ModifiedById = CustomHttpContext.GetUserLoginInfo().UserId;
            degreeMapped.ModifiedDate = GeneralService.CurrentDate;

            repo.Update(degreeMapped);
            await _genericUnitOfWork.SaveChangesAsync();
        }
    }
}
