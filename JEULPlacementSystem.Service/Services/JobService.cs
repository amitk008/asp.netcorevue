﻿using AutoMapper;
using JEULPlacementSystem.Common.Configuration;
using JEULPlacementSystem.Common.Constant;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Domain;
using JEULPlacementSystem.Service.Interface;
using Kendo.DynamicLinqCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service
{
    public class JobService : ParentService, IJobService
    {
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        private readonly IEmailService _emailService;
        private readonly IMapper _mapper; 
        public JobService(IGenericUnitOfWork genericUnitOfWork, IMapper mapper,
                               IEmailService emailService) : base(genericUnitOfWork)
        {
            _genericUnitOfWork = genericUnitOfWork;
            _mapper = mapper;
            _emailService = emailService;
        }

        public async Task CreateAsync(JobViewModel model, Guid tenantId)
        {
            var loggedInUserId = CustomHttpContext.GetUserLoginInfo().UserId;

            var repo = _genericUnitOfWork.GetRepository<Job, Guid>();
            var company = await _genericUnitOfWork.GetRepository<CompanyUser, Guid>().FirstOrDefaultAsync(x => x.UserId == loggedInUserId);

            var job = _mapper.Map<JobViewModel, Job>(model);
            job.Id = Guid.NewGuid();
            job.CreatedById = loggedInUserId;
            job.CreatedDate = GeneralService.CurrentUTCDate;
            job.TenantId = tenantId;
            job.CompanyId = company.CompanyId;
            job.IsApproved = true;
            repo.Add(job);



            await _genericUnitOfWork.SaveChangesAsync();


        }

        public async Task DeleteAsync(Guid id, Guid tenantId)
        {
            var job = await _genericUnitOfWork.GetRepository<Job, Guid>().FirstOrDefaultAsync(x => x.Id == id && x.TenantId == tenantId);

            if (job == null)
                throw new ArgumentException("Job does not exist");
            job.IsDeleted = true;
            job.IsActive = false;
            job.DeletedDate = DateTime.UtcNow;

            await _genericUnitOfWork.SaveChangesAsync();

        }

        public DataSourceResult GetAll(KendoDataRequest kendoDataRequest, Guid? tenantId)
        {
            var result = _genericUnitOfWork.GetRepository<Job, Guid>().GetAll(x => x.IsActive && !x.IsDeleted );
            
            if (tenantId!=null)
                result = result.Where(x =>  x.TenantId == tenantId);


            if (kendoDataRequest.ExternalFilter.ContainsKey("Title") && kendoDataRequest.ExternalFilter["Title"] != null)
            {
                var filterName = kendoDataRequest.ExternalFilter["Title"].ToString();
                result = result.Where(p => p.Title.Contains(filterName) );
            }

          
            return result
                    .ToDataSourceResult<Job, JobViewModel>(_mapper, kendoDataRequest, new Sort() { Field = "CreatedDate", Dir = "desc" });


        }

        public async Task<JobViewModel> GetAsync(Guid id)
        {
            var job = await _genericUnitOfWork.GetRepository<Job, Guid>().FirstOrDefaultAsync(x => x.Id == id);
            return _mapper.Map<Job, JobViewModel>(job);

        }

        public List<StudentListViewModel> GetStudentListByJob(Guid id, string name, Guid tenantId)
        {
            var repo = _genericUnitOfWork.GetRepository<JobApplied, Guid>();
            var students = repo.GetAll(x => x.JobId == id).Select(x => new StudentListViewModel()
            {
                Id = x.StudentId,
                FirstName = x.Student.User.FirstName,
                LastName = x.Student.User.LastName,
                IsSortListed = x.Job.JobSortLists.FirstOrDefault(c=>c.StudentId == x.StudentId )!=null ?
                                    x.Job.JobSortLists.FirstOrDefault(c => c.StudentId == x.StudentId).IsSortListed: false,
                IsEmailSent = x.Job.JobSortLists.FirstOrDefault(c => c.StudentId == x.StudentId) != null ?
                                    x.Job.JobSortLists.FirstOrDefault(c => c.StudentId == x.StudentId).IsEmailSent : false,
                FullName = x.Student.User.FullName,
            }).AsQueryable();
            if (!string.IsNullOrEmpty(name))
                students = students.Where(x => x.FullName.Contains(name));
        
            return students.ToList();

        }



        public async Task UpdateAsync(JobViewModel model, Guid tenantId)
        {
            var loggedInUserId = CustomHttpContext.GetUserLoginInfo().UserId;
            var repo = _genericUnitOfWork.GetRepository<Job, Guid>();
            var job = await repo.FirstOrDefaultAsync(x => x.Id == model.Id && x.TenantId == tenantId);

            if (job == null)
                throw new ArgumentException("Job does not exist");


            var jobMapped = _mapper.Map(model, job);

            jobMapped.ModifiedById = loggedInUserId;
            jobMapped.ModifiedDate = GeneralService.CurrentDate;


            repo.Update(jobMapped);
            await _genericUnitOfWork.SaveChangesAsync();

        }


        public async Task SortListStudents(Guid id, JobSortListViewModel model, Guid tenantId)
        {
            var loggedInUserId = CustomHttpContext.GetUserLoginInfo().UserId;
            var repo = _genericUnitOfWork.GetRepository<Job, Guid>();
            var job = await repo.FirstOrDefaultAsync(x => x.Id == id && x.TenantId == tenantId);


            if (job == null)
                throw new ArgumentException("Job does not exist");

            var pres = _genericUnitOfWork.GetRepository<JobSortListed, Guid>().GetAll(x => x.JobId == id).ToList().AsReadOnly();
          

             var jobSortList = job.JobSortLists.Where(x => x.JobId == id);

         

            if (!model.StudentList.Any())
                throw new ArgumentException("No Students");

            //if (!model.StudentList.Any(x => x.IsSortListed))
            //    throw new ArgumentException("Please sort list at least one student");

            var emailContent = GetEmailContentByTenantId(tenantId);


            model.StudentList.ForEach(student => {
                JobSortListed jSortListed;
                var sortListedEntity = jobSortList.FirstOrDefault(x => x.StudentId == student.Id);
                if (sortListedEntity == null)
                {
                    //create
                    jSortListed = new JobSortListed
                    {
                        IsSortListed = student.IsSortListed,
                        SortListedDate = GeneralService.CurrentUTCDate,
                        StudentId = student.Id,
                        IsEmailSent = model.SendEmail,
                    };
                    job.JobSortLists.Add(jSortListed);
                }
                else
                {

                    sortListedEntity.IsSortListed = student.IsSortListed;

                }
            });
            await _genericUnitOfWork.SaveChangesAsync();


            if (model.SendEmail)
            {
                var toEmailSortlisted = new List<StudentSortListedNotificationViewModel>();
                var toEmailnotSortlisted = new List<StudentSortListedNotificationViewModel>();
                foreach (var item in model.StudentList)
                {

                    var previousStudentItem = pres.FirstOrDefault(x => x.StudentId == item.Id);
                    //if (previousStudentItem == null || (previousStudentItem.IsSortListed != item.IsSortListed ))
                    //{
                        var info = await GetStudentEmailAndName(item.Id);

                        if (item.IsSortListed)
                            toEmailSortlisted.Add(new StudentSortListedNotificationViewModel { Email = info.Item1, FullName = info.Item2 });

                        else
                            toEmailnotSortlisted.Add(new StudentSortListedNotificationViewModel { Email = info.Item1, FullName = info.Item2 });
                    //}


                }

                if (toEmailSortlisted.Count > 0)
                {

                    toEmailSortlisted.ToList().ForEach(f =>
                    {
                        var emailObj = new StudentSortListedNotificationViewModel()
                        {
                            FullName = f.FullName,
                            Email = f.Email,
                            Body = emailContent.Result.SelectedEmailContent
                        };
                        _emailService.SendEmailAsync(f.Email, SENDGRIDEMAILTEMPLATE.SORTLISTED, emailObj);

                    });


                }
                if (toEmailnotSortlisted.Count > 0)
                {
                    toEmailnotSortlisted.ToList().ForEach(f =>
                    {
                        var emailObj = new StudentSortListedNotificationViewModel()
                        {
                            FullName = f.FullName,
                            Email = f.Email,
                            Body = emailContent.Result.NotSelectedEmailContent
                        };
                        _emailService.SendEmailAsync(f.Email, SENDGRIDEMAILTEMPLATE.NOTSORTLISTED, emailObj);

                    });
                }
                



            }


        }
    }
}
