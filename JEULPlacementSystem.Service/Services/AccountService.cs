﻿using JEULPlacementSystem.Common;
using JEULPlacementSystem.Common.Configuration;
using JEULPlacementSystem.Model.Common;
using JEULPlacementSystem.Service.Interface;
using JEULPlacementSystem.Common.Constant;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Domain;
using JEULPlacementSystem.Common.ViewModel.AppSettings;
using Microsoft.Extensions.Options;
using JEULPlacementSystem.Domain.Enum;
using System.Linq;
using System.Linq.Expressions; 
using System.Collections.Generic;
using System.Net;

namespace JEULPlacementSystem.Service
{
    public class AccountService : IAccountService
    {
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly UserManager<User> _userManager;
        private readonly IEmailService _emailService;
        private readonly IJwtFactory _jwtFactory;
        private readonly JEULSUrl _jeulsUrls;

        public AccountService(IGenericUnitOfWork genericUnitOfWork,
                              UserManager<User> userManager,
                              IJwtFactory jwtFactory,
                              JwtIssuerOptions jwtOptions,
                              IOptions<JEULSUrl> jeulsUrls,
                               IEmailService emailService)
        {
            _genericUnitOfWork = genericUnitOfWork;
            _jwtOptions = jwtOptions;
            _userManager = userManager;
            _jwtFactory = jwtFactory;
            _emailService = emailService;
            _jeulsUrls = jeulsUrls.Value;
        }
        public async Task<Response> Login(SignInViewModel model)
        {
            try
            {
                var identity = await GetClaimsIdentity(model.UserName, model.Password);
                if (identity == null)
                    throw new ArgumentException("Invalid username or password.");

                var user = await _userManager.FindByNameAsync(model.UserName);
                var roleId = await GetRoleId(user);

                var result = await Tokens.GenerateJwt(user, identity, _jwtFactory, _jwtOptions,
                    new JsonSerializerSettings { Formatting = Formatting.Indented });

                var menus = new List<UserPageViewModel>();
                var userwisePage = _genericUnitOfWork.GetRepository<UserPage, Guid>().GetAll()
                    .Where(c => c.RoleId == roleId && c.Page.ShowInView).Select(p => new UserPageViewModel
                    {
                        _name = "CSidebarNavItem",
                        PageDisplayOrder = p.Page.DisplayOrder,
                        PageId = p.Page.Id,
                        PageParentPageId = p.Page.ParentPageId,
                        to = p.Page.Path,
                        name = p.Page.Title,
                        icon = p.Page.Icon
                    }).ToList();
                userwisePage.Where(c => c.PageParentPageId == null).OrderBy(c => c.PageDisplayOrder).ToList().ForEach(
                    p =>
                    {
                        p.items = GetChildPages(userwisePage, p.PageId);
                        menus.Add(p);
                    });

                 menus.ToList().ForEach(f => f._name = f.items.Count > 0 ? "CSidebarNavDropdown" : "CSidebarNavItem");
                result.UserPages = menus;

                return new Response<SignInResponseViewModel> { Success = true, StatusCode = HttpStatusCode.OK, Content = result };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<Guid> GetRoleId(User user)
        {
            var roles = await _userManager.GetRolesAsync(user);
            if (roles.Count < 0)
                throw new ArgumentException("Invalid Role");

            var role = await _genericUnitOfWork.GetRepository<Role, Guid>().FirstOrDefaultAsync(x => x.Name == roles[0]);
            return role.Id;
        }

        private List<UserPageViewModel> GetChildPages(List<UserPageViewModel> pages, int? parentPageId)
        {
            var childpages = pages.Where(c => c.PageParentPageId == parentPageId).OrderBy(c => c.PageDisplayOrder);
            return childpages
                .Select(c => new UserPageViewModel
                {
                    _name = "CSidebarNavDropdown",
                    items = GetChildPages(pages, c.PageId),
                    PageDisplayOrder = c.PageDisplayOrder,
                    icon = c.icon,
                    PageId = c.PageId,
                    PageParentPageId = c.PageParentPageId,
                    to = c.to,
                    name = c.name
                }).ToList();
        }

        private async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return await Task.FromResult<ClaimsIdentity>(null);

            var userToVerify = await _userManager.FindByNameAsync(userName);

            // check the credentials
            if (await _userManager.CheckPasswordAsync(userToVerify, password))
            {
                if (!userToVerify.IsActive)
                    throw new ArgumentException("Your account is not active. Please contact administrator.");


                CheckRoleWiseAccess(userToVerify);

                var loginInfo = await GetLoginInfo(userToVerify);

                return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(userName, loginInfo));
            }

            return await Task.FromResult<ClaimsIdentity>(null);
        }

        private void CheckRoleWiseAccess(User user)
        {
            switch (user.UserType)
            {
                case UserType.University:
                    Expression<Func<UniversityUser, bool>> expression = x => x.User == user;
                    var university = _genericUnitOfWork.GetRepository<UniversityUser, Guid>().GetAll(expression).First();


                    if (!university.University.IsActive)
                        throw new Exception("University account is not active. Please contact administrator.");

                    break;

            }
        }

        private async Task<LoginInfo> GetLoginInfo(User userToVerify)
        {
            var role = await _userManager.GetRolesAsync(userToVerify);

            var roleName = await _genericUnitOfWork.GetRepository<Role, Guid>().FirstOrDefaultAsync(x => x.Name == role[0]);

            var loginInfo = new LoginInfo
            {
                UserId = userToVerify.Id,
                FullName = userToVerify.FullName,
                Email = userToVerify.Email,
                Role = roleName.Name,
                DisplayPicture = "",
                TenantId = SetTenantKey(userToVerify)
            };
            return loginInfo;
        }

        public async Task<Response> ResetPassword(ResetPasswordViewModel model)
        {
            var code = model.Code;
            code = HttpUtility.UrlDecode(code);
            code = code.Replace(" ", "+");
            if (string.IsNullOrEmpty(code))
                throw new ArgumentException("Invalid Code.");

            var user = await _userManager.FindByIdAsync(model.UserId.ToString());

            if (user == null)
                throw new ArgumentException("Invalid token.");

            var messsage = await _userManager.ResetPasswordAsync(user, code, model.Password);
            if (!messsage.Succeeded) throw new ArgumentException("Invalid Token");

            return new Response<string> { Success = true };
        }

        public async Task<Response> ForgotPassword(ForgotPasswordViewModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
                throw new ArgumentException("USER_DOES_NOT_EXIST");


            var code = await _userManager.GeneratePasswordResetTokenAsync(user);
            code = HttpUtility.UrlEncode(code);
            var userId = user.Id.ToString();
            userId = HttpUtility.UrlEncode(userId);

            var appUrl = _jeulsUrls.WebUrl;
            var emailObj = new ForgotPasswordNotificationViewModel
            {
                FullName = user.FullName,
                CallbackUrl = $"{appUrl}#/resetpassword?userId={userId}&code={code}",
                WebUrl = appUrl
            };




            await _emailService.SendEmailAsync(user.Email, SENDGRIDEMAILTEMPLATE.FORGOT_PASSWORD, emailObj);
            return new Response<string> { Success = true };
        }


        private Guid SetTenantKey(User user)
        {
            if (user.UserType == UserType.University)
                return user.Id;
            else if (user.UserType == UserType.Company)
                return user.CompanyUsers.First().Company.TenantId;

            else if (user.UserType == UserType.Student)
                return user.Students.First().University.UniversityUsers.First().UserId;

            return Guid.Empty;
        }

    }
}
