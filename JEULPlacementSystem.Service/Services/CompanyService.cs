﻿using AutoMapper;
using JEULPlacementSystem.Common.Configuration;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Domain;
using JEULPlacementSystem.Domain.Constant;
using JEULPlacementSystem.Domain.Enum;
using JEULPlacementSystem.Service.Interface;
using Kendo.DynamicLinqCore;
using System;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service
{
    public class CompanyService : ICompanyService
    {
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        private readonly IAccountService _accountService;
        private readonly IMapper _mapper;
        public CompanyService(IGenericUnitOfWork genericUnitOfWork, IMapper mapper, IAccountService accountService)
        {
            _genericUnitOfWork = genericUnitOfWork;
            _mapper = mapper;
            _accountService = accountService;
        }

        public async Task CreateAsync(CompanyViewModel model, Guid tenantId)
        {
            var loggedInUserId = CustomHttpContext.GetUserLoginInfo().UserId;

            var repo = _genericUnitOfWork.GetRepository<Company, string>();
            var userRepo = _genericUnitOfWork.GetRepository<User, Guid>();

            var emailExists = await userRepo.Exists(c => c.Email == model.PrimaryContactEmail.Trim());
            if (emailExists)
                throw new ArgumentException("User with the email already exists in the system");


            var Company = _mapper.Map<CompanyViewModel, Company>(model);
            Company.Id = Guid.NewGuid();
            Company.CreatedById = loggedInUserId;
            Company.CreatedDate = GeneralService.CurrentUTCDate;
            Company.TenantId = tenantId;

            repo.Add(Company);

            var user = new User
            {
                Id = Guid.NewGuid(),
                FullName = model.PrimaryContactName,
                UserName = model.PrimaryContactEmail,
                Email = model.PrimaryContactEmail,
                NormalizedEmail = model.PrimaryContactEmail.ToUpper(),
                NormalizedUserName = model.PrimaryContactEmail.ToUpper(),
                EmailConfirmed = true,
                UserType = UserType.Company,
                IsActive = true,
                SecurityStamp = Guid.NewGuid().ToString(), 
                CreatedDate = GeneralService.CurrentUTCDate,
            };
            userRepo.Add(user);

            _genericUnitOfWork.GetRepository<UserRole, Guid>().Add(new UserRole
            {
                UserId = user.Id,
                RoleId = new Guid(ApplicationConstants.CompanyRoleGuid)
            });
            CompanyUser CompanyUser = new CompanyUser
            {
                Company = Company,
                UserId = user.Id
            };

            Company.CompanyUsers.Add(CompanyUser);

            await _genericUnitOfWork.SaveChangesAsync();

            

            await _accountService.ForgotPassword(new ForgotPasswordViewModel() { Email = user.Email});

        }

        public async Task DeleteAsync(Guid id, Guid tenantId)
        {
            var Company = await _genericUnitOfWork.GetRepository<Company, Guid>().FirstOrDefaultAsync(x => x.Id == id && x.TenantId ==tenantId);

            if (Company == null)
                throw new ArgumentException("Company does not exist");
            Company.IsDeleted = true;
            Company.IsActive = false;
            Company.DeletedDate = DateTime.UtcNow;

            await _genericUnitOfWork.SaveChangesAsync();

        }

        public DataSourceResult GetAll(KendoDataRequest kendoDataRequest, Guid tenantId)
        {
            var universitites = _genericUnitOfWork.GetRepository<Company, string>().GetAll(x => x.IsActive && !x.IsDeleted && x.TenantId == tenantId);

            return universitites
                    .ToDataSourceResult<Company, CompanyViewModel>(_mapper, kendoDataRequest, new Sort() { Field = "CreatedDate", Dir = "desc" });


        }

        public async Task<CompanyViewModel> GetAsync(Guid id, Guid tenantId)
        {
            var Company = await _genericUnitOfWork.GetRepository<Company, Guid>().FirstOrDefaultAsync(x => x.Id == id);
            return _mapper.Map<Company, CompanyViewModel>(Company); ;

        }

        public async Task UpdateAsync(CompanyViewModel model, Guid tenantId)
        {
            var loggedInUserId = CustomHttpContext.GetUserLoginInfo().UserId;
            var repo = _genericUnitOfWork.GetRepository<Company, Guid>();
            var CompanyUserRepo = _genericUnitOfWork.GetRepository<CompanyUser, int>();
            var Company = await repo.FirstOrDefaultAsync(x => x.Id == model.Id );

            if (Company == null)
                throw new ArgumentException("Company does not exist");

            var CompanyUser = await CompanyUserRepo.FirstOrDefaultAsync(c => c.CompanyId == model.Id);

            if (CompanyUser == null)
                throw new ArgumentException("Company User doesnot exists");
 
               

            var CompanyMapped = _mapper.Map(model, Company);

            CompanyMapped.ModifiedById = loggedInUserId;
            CompanyMapped.ModifiedDate = GeneralService.CurrentDate;

            CompanyUser.User.FullName = model.PrimaryContactName;
           // CompanyMapped.ModifiedById = loggedInUserId;
            CompanyMapped.ModifiedDate = GeneralService.CurrentDate;



            repo.Update(CompanyMapped);
            await _genericUnitOfWork.SaveChangesAsync();

        }
    }
}
