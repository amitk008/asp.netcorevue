﻿using JEULPlacementSystem.Common.ViewModel.AppSettings;
using JEULPlacementSystem.Service.Interface;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service
{
    public class EmailService : IEmailService
    {
        private readonly EmailConfiguration _emailConfiguration;
        private readonly JEULSUrl _jeulsUrls;

        public EmailService(IOptions<EmailConfiguration> emailConfiguration,
            IOptions<JEULSUrl> jeulsUrls)
        {
            _emailConfiguration = emailConfiguration.Value;
            _jeulsUrls = jeulsUrls.Value;
        }

         
        public async Task SendEmailAsync(string toEmail, string templateId, object data)
        {
            try
            {
                var client = new SendGridClient(_emailConfiguration.ApiKey);
                var from = new EmailAddress(_emailConfiguration.FromEmail, _emailConfiguration.FromName);
                var to = new EmailAddress(toEmail, "recipient");
                var msg = MailHelper.CreateSingleTemplateEmail(from, to, templateId, data);

                var response = await client.SendEmailAsync(msg);
                //todo log response 

            }

            catch (Exception ex)
            {
            }
        }




    }
}