﻿using JEULPlacementSystem.Domain;
using System;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service
{
    public class ParentService
    {
        
        private readonly IGenericUnitOfWork _genericUnitOfWork;

        public ParentService(IGenericUnitOfWork genericUnitOfWork)
        {
            _genericUnitOfWork = genericUnitOfWork;
        }
        protected async Task<Guid> GetCompanyId(Guid userId)
        {
            var company = await _genericUnitOfWork.GetRepository<CompanyUser, Guid>().FirstOrDefaultAsync(x => x.UserId == userId);
            if (company != null) return company.CompanyId;
            throw new ArgumentException($"Company not Found");
        }

        protected async Task<Guid> GetUniversityId(Guid userId)
        {
            var university = await _genericUnitOfWork.GetRepository<UniversityUser, Guid>().FirstOrDefaultAsync(x => x.UserId == userId);
            if (university != null) return university.UniversityId;
            throw new ArgumentException($"University not Found");
        }


        protected async Task<int> GetStudentId(Guid userId)
        {
            var student = await _genericUnitOfWork.GetRepository<Student, int>().FirstOrDefaultAsync(x => x.UserId == userId);
            if (student != null) return student.Id;
            throw new ArgumentException($"Student not Found");
        }

        protected async Task<TKey> GetInternalIdAsync<TEntity, TKey>(TKey id) where TEntity : BaseEntity<TKey>
        {
            var repo = _genericUnitOfWork.GetRepository<TEntity, TKey>();
            var entity = await repo.FirstOrDefaultAsync(x => x.Id.Equals(id));
            if (entity == null) throw new ArgumentException("Invalid Id");
            return entity.Id;
        }

        protected async Task <Tuple<string,string>> GetStudentEmailAndName(int id)
        {
            var student = await _genericUnitOfWork.GetRepository<Student, int>().FirstOrDefaultAsync(x => x.Id == id);
            if (student != null) return new Tuple<string,string>(student.User.Email, student.User.FullName);
            throw new ArgumentException($"Student not Found");
        }

        protected async Task<EmailContent> GetEmailContentByTenantId(Guid id)
        {
            var emailContent = await _genericUnitOfWork.GetRepository<EmailContent, Guid>().FirstOrDefaultAsync(x => x.TenantId == id);
            if (emailContent != null) return emailContent;
            return new EmailContent();
        }
    }
}
