﻿using AutoMapper;
using JEULPlacementSystem.Common.Configuration;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Domain;
using JEULPlacementSystem.Domain.Constant;
using JEULPlacementSystem.Domain.Enum;
using JEULPlacementSystem.Service.Interface;
using Kendo.DynamicLinqCore;
using System;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service
{
    public class UniversityService : IUniversityService
    {
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        private readonly IAccountService _accountService;
        private readonly IMapper _mapper;
        public UniversityService(IGenericUnitOfWork genericUnitOfWork, IMapper mapper, IAccountService accountService)
        {
            _genericUnitOfWork = genericUnitOfWork;
            _mapper = mapper;
            _accountService = accountService;
        }

       

        public async Task CreateAsync(UniversityViewModel model)
        {
            var loggedInUserId = CustomHttpContext.GetUserLoginInfo().UserId;

            var repo = _genericUnitOfWork.GetRepository<University, string>();
            var userRepo = _genericUnitOfWork.GetRepository<User, Guid>();

            var emailExists = await userRepo.Exists(c => c.Email == model.PrimaryContactEmail.Trim());
            if (emailExists)
                throw new ArgumentException("User with the email already exists in the system");



            var university = _mapper.Map<UniversityViewModel, University>(model);
            university.Id = Guid.NewGuid();
            university.CreatedById = loggedInUserId;
            university.CreatedDate = GeneralService.CurrentUTCDate;

            repo.Add(university);

            var user = new User
            {
                Id = Guid.NewGuid(),
                FullName = model.PrimaryContactName,
                UserName = model.PrimaryContactEmail,
                Email = model.PrimaryContactEmail,
                NormalizedEmail = model.PrimaryContactEmail.ToUpper(),
                NormalizedUserName = model.PrimaryContactEmail.ToUpper(),
                EmailConfirmed = true,
                UserType = UserType.University,
                IsActive = true,
                SecurityStamp = Guid.NewGuid().ToString(), 
                CreatedDate = GeneralService.CurrentUTCDate
            };
            userRepo.Add(user);

            _genericUnitOfWork.GetRepository<UserRole, Guid>().Add(new UserRole
            {
                UserId = user.Id,
                RoleId = new Guid(ApplicationConstants.UniversityRoleGuid)
            });


            UniversityUser universityUser = new UniversityUser
            {
                University = university,
                UserId = user.Id
            };

            university.UniversityUsers.Add(universityUser);


            await _genericUnitOfWork.SaveChangesAsync();

            

            await _accountService.ForgotPassword(new ForgotPasswordViewModel() { Email = user.Email});

        }

        public async Task DeleteAsync(Guid id)
        {
            var university = await _genericUnitOfWork.GetRepository<University, Guid>().FirstOrDefaultAsync(x => x.Id == id);

            if (university == null)
                throw new ArgumentException("University does not exist");
            university.IsDeleted = true;
            university.IsActive = false;
            university.DeletedDate = DateTime.UtcNow;

            await _genericUnitOfWork.SaveChangesAsync();

        }


        public async Task<UniversityViewModel> Approve(Guid id)
        {
            var loggedInUserId = CustomHttpContext.GetUserLoginInfo().UserId;
            var university = await _genericUnitOfWork.GetRepository<University, Guid>().FirstOrDefaultAsync(x => x.Id == id);

            if (university == null)
                throw new ArgumentException("University does not exist");

            university.IsApproved = !university.IsApproved;
            if (university.IsApproved)
            {
                university.ApprovedById = loggedInUserId;
                university.ApprovedDate = GeneralService.CurrentUTCDate;
            }

            await _genericUnitOfWork.SaveChangesAsync();
            return _mapper.Map<University, UniversityViewModel>(university);
        }

        public async Task<UniversityViewModel> ActivateDeactivateUniversity(Guid id)
        {
            var loggedInUserId = CustomHttpContext.GetUserLoginInfo().UserId;
            var university = await _genericUnitOfWork.GetRepository<University, Guid>().FirstOrDefaultAsync(x => x.Id == id);

            if (university == null)
                throw new ArgumentException("University does not exist");

            university.IsActive = !university.IsActive;

            university.ModifiedById = loggedInUserId;
            university.ModifiedDate = GeneralService.CurrentUTCDate;

            await _genericUnitOfWork.SaveChangesAsync();
            return _mapper.Map<University, UniversityViewModel>(university);
        }

        public DataSourceResult GetAll(KendoDataRequest kendoDataRequest)
        {
            var universitites = _genericUnitOfWork.GetRepository<University, string>().GetAll();

            return universitites
                    .ToDataSourceResult<University, UniversityListViewModel>(_mapper, kendoDataRequest, new Sort() { Field = "CreatedDate", Dir = "desc" });


        }

        public async Task<UniversityViewModel> GetAsync(Guid id)
        {
            var university = await _genericUnitOfWork.GetRepository<University, Guid>().FirstOrDefaultAsync(x => x.Id == id);
            return _mapper.Map<University, UniversityViewModel>(university); 

        }

        public async Task UpdateAsync(UniversityViewModel model)
        {
            var loggedInUserId = CustomHttpContext.GetUserLoginInfo().UserId;
            var repo = _genericUnitOfWork.GetRepository<University, Guid>();
            var universityUserRepo = _genericUnitOfWork.GetRepository<UniversityUser, int>();
            var university = await repo.FirstOrDefaultAsync(x => x.Id == model.Id);

            if (university == null)
                throw new ArgumentException("University does not exist");

            var universityUser = await universityUserRepo.FirstOrDefaultAsync(c => c.UniversityId == model.Id);

            if (universityUser == null)
                throw new ArgumentException("University User doesnot exists");
 
               

            var universityMapped = _mapper.Map(model, university);

            universityMapped.ModifiedById = loggedInUserId;
            universityMapped.ModifiedDate = GeneralService.CurrentDate;

            universityUser.User.FullName = model.PrimaryContactName;
           // universityMapped.ModifiedById = loggedInUserId;
            universityMapped.ModifiedDate = GeneralService.CurrentDate;



            repo.Update(universityMapped);
            await _genericUnitOfWork.SaveChangesAsync();

        }
    }
}
