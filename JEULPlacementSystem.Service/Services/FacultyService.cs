﻿using AutoMapper;
using JEULPlacementSystem.Common.Configuration;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Domain;
using JEULPlacementSystem.Service.Interface;
using Kendo.DynamicLinqCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service
{
    public class FacultyService : IFacultyService
    {
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        private readonly IMapper _mapper;
        public FacultyService(IGenericUnitOfWork genericUnitOfWork, IMapper mapper)
        {
            _genericUnitOfWork = genericUnitOfWork;
            _mapper = mapper;
        }
        public async Task CreateAsync(FacultyViewModel model, Guid tenantId)
        {
            var repo = _genericUnitOfWork.GetRepository<Faculty, string>();

            var faculty = _mapper.Map<FacultyViewModel, Faculty>(model);
            faculty.Id = Guid.NewGuid();
            faculty.CreatedById = CustomHttpContext.GetUserLoginInfo().UserId;
            faculty.CreatedDate = GeneralService.CurrentDate;
            faculty.TenantId = tenantId;

            repo.Add(faculty);

            await _genericUnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id, Guid tenantId)
        {
            var faculty = await _genericUnitOfWork.GetRepository<Faculty, Guid>().FirstOrDefaultAsync(x => x.Id == id && x.TenantId == tenantId);

            if (faculty == null)
                throw new ArgumentException("Faculty does not exist");
            faculty.IsActive = false;
            faculty.IsDeleted = true;
            faculty.DeletedDate = GeneralService.CurrentDate;

            await _genericUnitOfWork.SaveChangesAsync();

        }

        public DataSourceResult GetAll(KendoDataRequest kendoDataRequest, Guid tenantId)
        {
            var faculties = _genericUnitOfWork.GetRepository<Faculty, string>().GetAll(x => x.IsActive && !x.IsDeleted && x.TenantId == tenantId);


            return faculties.ToDataSourceResult<Faculty, FacultyViewModel>(_mapper, kendoDataRequest, new Sort() { Field = "CreatedDate", Dir = "desc" });
        }

        public async Task<FacultyViewModel> GetAsync(Guid id, Guid tenantId)
        {
            var faculty = await _genericUnitOfWork.GetRepository<Faculty, Guid>().FirstOrDefaultAsync(x => x.Id == id && x.TenantId == tenantId);
            return _mapper.Map<Faculty, FacultyViewModel>(faculty); ;
        }

        public async Task UpdateAsync(FacultyViewModel model, Guid tenantId)
        {
            var repo = _genericUnitOfWork.GetRepository<Faculty, Guid>();
            var faculty = await repo.FirstOrDefaultAsync(x => x.Id == model.Id && x.TenantId == tenantId);

            if (faculty == null)
                throw new ArgumentException("Faculty does not exist");



            var facultyMapped = _mapper.Map(model, faculty);

            facultyMapped.ModifiedById = CustomHttpContext.GetUserLoginInfo().UserId;
            facultyMapped.ModifiedDate = GeneralService.CurrentDate;

            repo.Update(facultyMapped);
            await _genericUnitOfWork.SaveChangesAsync();
        }

        public IList<ListViewModel> ListFaculty(Guid tenantId)
        {
            var repo = _genericUnitOfWork.GetRepository<Faculty, Guid>();

            Expression<Func<Faculty, bool>> expression = c =>  c.IsActive && !c.IsDeleted && c.TenantId == tenantId;

            var result =
                _mapper.Map<IList<Faculty>, IList<ListViewModel>>(repo.GetAll(expression).OrderBy(o => o.Name)
                    .ToList());
            return result;
        }
    }
}
