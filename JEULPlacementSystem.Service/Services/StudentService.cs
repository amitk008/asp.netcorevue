﻿using AutoMapper;
using DinkToPdf.Contracts;
using JEULPlacementSystem.Common.Configuration;
using JEULPlacementSystem.Common.Constant;
using JEULPlacementSystem.Common.Helpers;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Domain;
using JEULPlacementSystem.Domain.Constant;
using JEULPlacementSystem.Domain.Enum;
using JEULPlacementSystem.Service.Infrastructure;
using JEULPlacementSystem.Service.Interface;
using Kendo.DynamicLinqCore;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service
{
    public class StudentService : ParentService, IStudentService
    {
     
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        private readonly IEmailService _emailService;
        private readonly IMapper _mapper;
        private readonly IConverter _converter;
        private readonly ViewRender _viewRender;
        public StudentService(IGenericUnitOfWork genericUnitOfWork, IMapper mapper, ViewRender viewRender, IConverter converter
                                                , IEmailService emailService) : base(genericUnitOfWork)
        {
            _genericUnitOfWork = genericUnitOfWork;
            _mapper = mapper;
            _viewRender = viewRender;
            _converter = converter;
            _emailService = emailService;
     
        }


        public async Task Registration(StudentViewModel model)
        {


            var repo = _genericUnitOfWork.GetRepository<User, Guid>();

            var studentTempRepo = await _genericUnitOfWork.GetRepository<StudentTempRegistration, Guid>().
                                        FirstOrDefaultAsync(x => x.Email == model.Email && x.Status == StudentRegistrationStatus.Pending);



            var existingEmail = repo.GetAll(x => x.Email == model.Email);
            if (existingEmail.Any())
                throw new ArgumentException("Email Already Exists!");

            if (studentTempRepo == null)
                throw new ArgumentException("Email Address Not Verified/Approved!");



            var existingPhoneNumber = repo.GetAll(x => x.PhoneNumber == model.PhoneNumber);
            if (existingPhoneNumber.Any())
                throw new ArgumentException("Phone Number Already Exists!");

            var mappedUser = _mapper.Map<StudentViewModel, User>(model);
            mappedUser.Id = Guid.NewGuid();
            mappedUser.UserName = mappedUser.Email;
            mappedUser.UserType = UserType.Student;
            mappedUser.SecurityStamp = Guid.NewGuid().ToString();
            mappedUser.PasswordHash = new PasswordHasher<User>().HashPassword(mappedUser, model.Password);
            mappedUser.EmailConfirmed = true;

            mappedUser.CreatedDate = GeneralService.CurrentDate;

            _genericUnitOfWork.GetRepository<Student, int>().Add(new Student()
            {
                UserId = mappedUser.Id,
                IsActive = true,
                IsApproved = true,
                UniversityId = studentTempRepo.UniversityId,
                TenantId = studentTempRepo.TenantId,
                CreatedDate = GeneralService.CurrentUTCDate
            });

            _genericUnitOfWork.GetRepository<UserRole, Guid>().Add(new UserRole
            {
                UserId = mappedUser.Id,
                RoleId = new Guid(ApplicationConstants.StudentRoleGuid)
            });

            repo.Add(mappedUser);

            studentTempRepo.Status = StudentRegistrationStatus.Approved;




            await _genericUnitOfWork.SaveChangesAsync();


        }


        public async Task<DataSourceResult> OnBoardingRegistration(List<StudentOnBoardingViewModel> studentList, Guid tenantId)
        {
            if (studentList.Count == 0)
                throw new Exception("RECORD_NOT_FOUND");

            List<StudentTempRegistration> studentRegistrations = new List<StudentTempRegistration>();
            var loggedInUserId = CustomHttpContext.GetUserLoginInfo().UserId;

            if (studentList.GroupBy(x => x.Email).Any(g => g.Count() > 1))
                throw new Exception("DUPLICATE EMAIL ID ON THE LIST ");

            var studentTempList = _genericUnitOfWork.GetRepository<StudentTempRegistration, Guid>()
                                            .GetAll();
            var userList = _genericUnitOfWork.GetRepository<User, Guid>().GetAll();
            foreach (StudentOnBoardingViewModel item in studentList)
            {
                if (!RegexUtilities.IsValidEmail(item.Email))
                {
                    item.Status = "Invalid Email Address";
                    continue;
                }

                var emailExists = userList.Any(x => x.Email == item.Email.Trim());
                var studentTempEmailExists = studentTempList.Any(x => x.Email == item.Email.Trim());
                if (emailExists || studentTempEmailExists)
                {
                    item.Status = "Email Address already Exists";
                    continue;
                }
                item.Status = "Success";

                studentRegistrations.Add(new StudentTempRegistration
                {
                    Id = Guid.NewGuid(),
                    Email = item.Email,
                    UniversityId = await GetUniversityId(loggedInUserId),
                    Status = StudentRegistrationStatus.Pending,
                    TenantId = tenantId

                });
            }

            if (studentList.All(x => x.Status == "Success"))
            {
                await _genericUnitOfWork.GetRepository<StudentTempRegistration, Guid>().AddRangeAsync(studentRegistrations);
                await _genericUnitOfWork.SaveChangesAsync();
            }


            return new DataSourceResult
            {
                Data = studentList,
                Total = studentList.Count,
                Aggregates = null
            };

        }



        public DataSourceResult GetAll(KendoDataRequest kendoDataRequest, Guid tenantId)
        {
            var students = _genericUnitOfWork.GetRepository<Student, int>()
                                .GetAll(x => x.IsActive && x.TenantId == tenantId);

            return students
                    .ToDataSourceResult<Student, StudentListViewModel>(_mapper, kendoDataRequest, new Sort() { Field = "CreatedDate", Dir = "desc" });


        }

        public async Task<StudentViewModel> GetAsync(int id, Guid tenantId)
        {
            var student = await _genericUnitOfWork.GetRepository<Student, int>().FirstOrDefaultAsync(x => x.Id == id);
            return _mapper.Map<Student, StudentViewModel>(student); ;

        }

        public async Task<List<StudentJobListViewModel>> GetAllJobList(string title, Guid tenantId)
        {
            var currentDate = GeneralService.CurrentUTCDate;
            var loggedInUserId = CustomHttpContext.GetUserLoginInfo().UserId;

            int studentId = await GetStudentId(loggedInUserId);

            Expression<Func<Job, bool>> expression = x => currentDate.Date <= x.EndDate.Date && x.IsActive
                                                                && x.IsApproved && !x.IsDeleted && x.TenantId == tenantId;
            var jobList = _genericUnitOfWork.GetRepository<Job, Guid>().GetAll(expression);
            if (!string.IsNullOrEmpty(title))
                jobList = jobList.Where(x => x.Title.Contains(title));
            var jobAppliedList = _genericUnitOfWork.GetRepository<JobApplied, int>().GetAll();
            var jobSavedList = _genericUnitOfWork.GetRepository<JobSaved, int>().GetAll();
            var result = _mapper.Map<List<Job>, List<StudentJobListViewModel>>(jobList.ToList());


            result.ForEach(x =>
            {
                x.IsApplied = jobAppliedList.Any(j => j.JobId == x.Id && j.StudentId == studentId);
                x.IsSaved = jobSavedList.Any(j => j.JobId == x.Id && j.StudentId == studentId);
            }


            );
            return result;


        }

        public async Task ApplyJob(JobCounterViewModel model)
        {
            model.StudentId = await GetStudentId(CustomHttpContext.GetUserLoginInfo().UserId);
            var repo = _genericUnitOfWork.GetRepository<JobApplied, int>();
            var job = await repo.FirstOrDefaultAsync(x => x.JobId == model.Id && x.StudentId == model.StudentId);

            if (job != null)
                throw new ArgumentException("YOU HAVE ALREADY APPLIED TO THIS JOB");

            repo.Add(
                    new JobApplied()
                    {
                        JobId = model.Id,
                        StudentId = model.StudentId,
                        AppliedDate = GeneralService.CurrentUTCDate
                    });
            await _genericUnitOfWork.SaveChangesAsync();

            var emailObj = new
            {

            };
            await _emailService.SendEmailAsync(job.Job.Company.CompanyUsers.FirstOrDefault().User.Email, SENDGRIDEMAILTEMPLATE.JOBAPPLIED, emailObj);
        }


        public async Task UpdateViewCountAsync(JobCounterViewModel model)
        {
            model.StudentId = await GetStudentId(CustomHttpContext.GetUserLoginInfo().UserId);
            var repo = _genericUnitOfWork.GetRepository<JobViewCount, int>();
            var job = await repo.FirstOrDefaultAsync(x => x.JobId == model.Id && x.StudentId == model.StudentId);

            if (job == null)
            {
                repo.Add(
                        new JobViewCount()
                        {
                            JobId = model.Id,
                            StudentId = model.StudentId
                        });
                await _genericUnitOfWork.SaveChangesAsync();
            }

        }

        public async Task<bool> SaveJobAsync(JobCounterViewModel model)
        {
            model.StudentId = await GetStudentId(CustomHttpContext.GetUserLoginInfo().UserId);
            var repo = _genericUnitOfWork.GetRepository<JobSaved, int>();
            var job = await repo.FirstOrDefaultAsync(x => x.JobId == model.Id && x.StudentId == model.StudentId);
            bool result = false;
            if (job != null)
            {
                repo.Delete(job);

            }
            else
            {
                repo.Add(
                        new JobSaved()
                        {
                            JobId = model.Id,
                            StudentId = model.StudentId,
                            CreatedDate = GeneralService.CurrentUTCDate
                        });

                result = true;
            }
            await _genericUnitOfWork.SaveChangesAsync();
            return result;
        }

        public async Task AddUpdateProfileAsync(MyProfileViewModel model, Guid tenantId)
        {
            var studentId = CustomHttpContext.GetUserLoginInfo().UserId;
            var repo = _genericUnitOfWork.GetRepository<Student, int>();
            var entity = await repo.FirstOrDefaultAsync(x => x.UserId == studentId && x.TenantId == tenantId);
            if (entity == null)
                throw new Exception("Student Does not Exist");
            switch (model.Action)
            {

                case ProfileAction.Default:

                    StudentProfile sProfile;

                    if (entity.StudentProfile == null)
                    {

                        //create
                        sProfile = new StudentProfile
                        {
                            Gender = model.StudentProfile.Gender,
                            MobileNumber = model.StudentProfile.MobileNumber,
                            DateOfBirth = model.StudentProfile.DateOfBirth,
                            MartialStatusId = model.StudentProfile.MartialStatusId,
                            Nationality = model.StudentProfile.Nationality,
                            JobTypeId = (JobType)model.StudentProfile.JobTypeId,
                            CurrentSalary = model.StudentProfile.CurrentSalary,
                            Skills = model.StudentProfile.Skills,
                            Objective = model.StudentProfile.Objective,
                            PermanentAddress = model.StudentProfile.PermanentAddress,
                            CurrentAddress = model.StudentProfile.CurrentAddress,
                            StudentId = entity.Id,
                            ExpectSalary = model.StudentProfile.ExpectSalary,

                        };
                        entity.StudentProfile = sProfile;
                    }
                    else
                    {
                        entity.StudentProfile.Gender = model.StudentProfile.Gender;
                        entity.StudentProfile.MobileNumber = model.StudentProfile.MobileNumber;
                        entity.StudentProfile.DateOfBirth = model.StudentProfile.DateOfBirth;
                        entity.StudentProfile.MartialStatusId = model.StudentProfile.MartialStatusId;
                        entity.StudentProfile.Nationality = model.StudentProfile.Nationality;
                        entity.StudentProfile.JobTypeId = (JobType)model.StudentProfile.JobTypeId;
                        entity.StudentProfile.CurrentSalary = model.StudentProfile.CurrentSalary;
                        entity.StudentProfile.Skills = model.StudentProfile.Skills;
                        entity.StudentProfile.Objective = model.StudentProfile.Objective;
                        entity.StudentProfile.PermanentAddress = model.StudentProfile.PermanentAddress;
                        entity.StudentProfile.CurrentAddress = model.StudentProfile.CurrentAddress;
                        entity.StudentProfile.StudentId = entity.Id;
                        entity.StudentProfile.ExpectSalary = model.StudentProfile.ExpectSalary;
                    }

                    entity.User.FirstName = model.StudentProfile.FirstName;
                    entity.User.LastName = model.StudentProfile.LastName;
                    entity.User.FullName = string.Join(" ", model.StudentProfile.FirstName, model.StudentProfile.LastName);
                    break;

                case ProfileAction.Education:

                    entity.StudentEducationDetails.ToList().ForEach(x => x.IsActive = false);
                    foreach (var studentEducation in model.StudentEducationDetails)
                    {
                        StudentEducationDetail sEducationDetail;
                        if (studentEducation.Id == default)
                        {
                            //create
                            sEducationDetail = new StudentEducationDetail
                            {
                                Degree = studentEducation.Degree,
                                Faculty = studentEducation.Faculty,
                                Institute = studentEducation.Institute,
                                StartDate = studentEducation.StartDate,
                                EndDate = studentEducation.EndDate,
                                GradingTypeId = (GradingType)studentEducation.GradingTypeId,
                                Marks = studentEducation.Marks,
                                IsActive = true

                            };
                            entity.StudentEducationDetails.Add(sEducationDetail);
                        }
                        else
                        {
                            //update
                            var internalId = await _genericUnitOfWork.GetRepository<StudentEducationDetail, int>()
                                            .FirstOrDefaultAsync(x => x.Id == studentEducation.Id);

                            sEducationDetail = entity.StudentEducationDetails.First(x => x.Id == studentEducation.Id);

                            if (sEducationDetail == null)
                                throw new ArgumentException("Detail Does not Exists");

                            sEducationDetail.Faculty = studentEducation.Faculty;
                            sEducationDetail.Degree = studentEducation.Degree;
                            sEducationDetail.Institute = studentEducation.Institute;
                            sEducationDetail.StartDate = studentEducation.StartDate;
                            sEducationDetail.EndDate = studentEducation.EndDate;
                            sEducationDetail.GradingTypeId = (GradingType)studentEducation.GradingTypeId;
                            sEducationDetail.Marks = studentEducation.Marks;
                            sEducationDetail.IsActive = true;
                        }
                    }

                    break;

                case ProfileAction.Experience:

                    entity.StudentExperienceDetails.ToList().ForEach(x => x.IsActive = false);
                    foreach (var studentExperience in model.StudentExperienceDetails)
                    {
                        StudentExperienceDetail sExperienceDetail;
                        if (studentExperience.Id == default)
                        {
                            //create
                            sExperienceDetail = new StudentExperienceDetail
                            {
                                CompanyName = studentExperience.CompanyName,
                                JobTitle = studentExperience.JobTitle,
                                JobTypeId = (JobType)studentExperience.JobTypeId,
                                StartDate = studentExperience.StartDate,
                                EndDate = studentExperience.EndDate,
                                IsCurrentJob = studentExperience.IsCurrentJob,
                                Responsibilities = studentExperience.Responsibilities,
                                IsActive = true

                            };
                            entity.StudentExperienceDetails.Add(sExperienceDetail);
                        }
                        else
                        {
                            //update
                            var internalId = await _genericUnitOfWork.GetRepository<StudentExperienceDetail, int>()
                                            .FirstOrDefaultAsync(x => x.Id == studentExperience.Id);

                            sExperienceDetail = entity.StudentExperienceDetails.First(x => x.Id == studentExperience.Id);

                            if (sExperienceDetail == null)
                                throw new ArgumentException("Expreience Detail Does not Exists");

                            sExperienceDetail.CompanyName = studentExperience.CompanyName;
                            sExperienceDetail.JobTitle = studentExperience.JobTitle;
                            sExperienceDetail.JobTypeId = (JobType)studentExperience.JobTypeId;
                            sExperienceDetail.StartDate = studentExperience.StartDate;
                            sExperienceDetail.EndDate = studentExperience.EndDate;
                            sExperienceDetail.IsCurrentJob = studentExperience.IsCurrentJob;
                            sExperienceDetail.Responsibilities = studentExperience.Responsibilities;
                            sExperienceDetail.IsActive = true;
                        }
                    }
                    break;
            }
            await _genericUnitOfWork.SaveChangesAsync();
        }

        public async Task<MyProfileViewModel> GetProfileAsync(Guid currentUserId, Guid tenantId)
        {

            var repo = _genericUnitOfWork.GetRepository<Student, int>();
            var entity = await repo.FirstOrDefaultAsync(x => x.UserId == currentUserId && x.TenantId == tenantId);

            if (entity == null)
                throw new Exception("Student Does not Exist");

            var response = _mapper.Map<Student, MyProfileViewModel>(entity);
            response.StudentProfile = response.StudentProfile == null ? new StudentProfileViewModel() : response.StudentProfile;
            response.StudentProfile.FirstName = entity.User.FirstName;
            response.StudentProfile.LastName = entity.User.LastName;
            return response;



        }

        public async Task<MyProfileViewModel> GetDetails(int id, Guid tenantId)
        {
            var repo = _genericUnitOfWork.GetRepository<Student, int>();
            var entity = await repo.FirstOrDefaultAsync(x => x.Id == id && x.TenantId == tenantId);

            if (entity == null)
                throw new Exception("Student Does not Exist");

            var response = _mapper.Map<Student, MyProfileViewModel>(entity);
            response.StudentProfile = response.StudentProfile == null ? new StudentProfileViewModel() : response.StudentProfile;
            response.StudentProfile.FirstName = entity.User.FirstName;
            response.StudentProfile.LastName = entity.User.LastName;
            return response;
        }

        public async Task<Tuple<MyProfileViewModel, string>> DownloadResume(int id, Guid tenantId)
        {
            var studentDetail = await GetDetails(id, tenantId);
            
            //var bytes = await pdf.BuildFile();
           // var html = _viewRender.Render("PDFReport/CVResume", studentDetail);

           
            //var byteArray = PDFBuilder.ConvertHtmltoPdf(_converter, html);
            return new Tuple<MyProfileViewModel, string>(studentDetail, string.Join(" ", studentDetail.StudentProfile.FirstName, studentDetail.StudentProfile.LastName));
        }
        
    }



}

