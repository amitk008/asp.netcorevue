﻿using System;
using AutoMapper;
using System.Threading.Tasks;
using JEULPlacementSystem.Domain;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Service.Interface;
using JEULPlacementSystem.Common.Configuration;

namespace JEULPlacementSystem.Service
{
    public class SettingsService : ParentService, ISettingsService
    {
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        private readonly IMapper _mapper;
        public SettingsService(IGenericUnitOfWork genericUnitOfWork, IMapper mapper) : base(genericUnitOfWork)
        {
            _genericUnitOfWork = genericUnitOfWork;
            _mapper = mapper;
        }

        public async Task AddEditEmailTemplate(EmailTemplateViewModel model, Guid tenantId)
        {

            var loggedInUserId = CustomHttpContext.GetUserLoginInfo().UserId;

            var repo = _genericUnitOfWork.GetRepository<EmailContent, int>();

            var entity = await repo.FirstOrDefaultAsync(c => c.TenantId == tenantId);

            if (entity == null)
            {
                repo.Add(new EmailContent
                {
                    CompanyId = await GetCompanyId(loggedInUserId),
                    IsActive = true,
                    IsApproved = true,
                    SelectedEmailContent = model.SelectedEmailContent,
                    NotSelectedEmailContent = model.NotSelectedEmailContent,
                    CreatedDate = GeneralService.CurrentUTCDate,
                    CreatedById = loggedInUserId,
                    TenantId = tenantId,
                });
            }
            else
            {

                entity.SelectedEmailContent = model.SelectedEmailContent;
                entity.NotSelectedEmailContent = model.NotSelectedEmailContent;
                entity.ModifiedDate = GeneralService.CurrentUTCDate;
                entity.ModifiedById = loggedInUserId;
            }



            await _genericUnitOfWork.SaveChangesAsync();


        }



        public async Task<EmailTemplateViewModel> GetEmailTemplateAsync(Guid tenantId)
        {
            var result = await _genericUnitOfWork.GetRepository<EmailContent, int>().FirstOrDefaultAsync(x => x.TenantId == tenantId);

            if (result == null)
                return new EmailTemplateViewModel();
            else
                return new EmailTemplateViewModel()
                {
                    NotSelectedEmailContent = result.NotSelectedEmailContent,
                    SelectedEmailContent = result.SelectedEmailContent
                };

        }


    }
}
