﻿using AutoMapper;
using JEULPlacementSystem.Common.Configuration;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Domain;
using JEULPlacementSystem.Domain.Enum;
using JEULPlacementSystem.Model.Common;
using JEULPlacementSystem.Service.Interface;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service
{
    public class ProfileService : IProfileService
    {
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        private readonly IUniversityService _universityService;
        private readonly ICompanyService _companyService;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly IStudentService _studentService;

        public ProfileService(IGenericUnitOfWork genericUnitOfWork, IMapper mapper, UserManager<User> userManager,
                               IUniversityService universityService,
                               ICompanyService companyService,
                               IStudentService studentService)
        {
            _genericUnitOfWork = genericUnitOfWork;
            _mapper = mapper;
            _userManager = userManager;
            _universityService = universityService;
            _companyService = companyService;
            _studentService = studentService;
        }


        public async Task<Response> UpdateProfile(MyProfileViewModel model, Guid tenantId)
        {
            var loggedInUserId = CustomHttpContext.GetUserLoginInfo().UserId;
            var user = await _userManager.FindByIdAsync(loggedInUserId.ToString());

            if (user == null)
                throw new ArgumentException("USER_DOES_NOT_EXIST");

            switch (user.UserType)
            {

                case UserType.University:
                    var university = await _genericUnitOfWork.GetRepository<UniversityUser, int>().FirstOrDefaultAsync(c => c.UserId == user.Id);

                    if (university == null)
                        throw new ArgumentException("University User doesnot exists");
                    var universityModel = new UniversityViewModel()
                    {
                        Id = university.UniversityId,
                        Name = model.Name,
                        Email= model.Email,
                        Address = model.Address,
                        Website = model.Website,
                        PrimaryContactName = model.PrimaryContactName
                    };
                    await _universityService.UpdateAsync(universityModel);

                    break;

                case UserType.Company:
                    var company = await _genericUnitOfWork.GetRepository<CompanyUser, int>().FirstOrDefaultAsync(c => c.UserId == user.Id);

                    if (company == null)
                        throw new ArgumentException("University User doesnot exists");
                    var companyModel = new CompanyViewModel()
                    {
                        Id = company.UserId,
                        Name = model.Name,
                        Email = model.Email,
                        Address = model.Address,
                        Website = model.Website,
                        PrimaryContactName = model.PrimaryContactName
                    };
                    await _companyService.UpdateAsync(companyModel, tenantId);
                    break;

                case UserType.Student:
                     await _studentService.AddUpdateProfileAsync(model, tenantId);
                    break;
            }
            return new Response<string> { Success = true };

        }

        public async Task<MyProfileViewModel> GetAsync(Guid tenantId)
        {
            var loggedInUserId = CustomHttpContext.GetUserLoginInfo().UserId;
            var user = await _userManager.FindByIdAsync(loggedInUserId.ToString());

            if (user == null)
                throw new ArgumentException("USER_DOES_NOT_EXIST");

            var response = new MyProfileViewModel();
            switch (user.UserType)
            {

                case UserType.University:
                    var universityUserRepo = _genericUnitOfWork.GetRepository<UniversityUser, int>();
                    var universityUser = await universityUserRepo.FirstOrDefaultAsync(c => c.UserId == user.Id);

                    if (universityUser == null)
                        throw new ArgumentException("University User doesnot exists");
                   
                    var universityModel = await _universityService.GetAsync(universityUser.UniversityId);

                    response = new MyProfileViewModel()
                    {
                         
                        Name = universityModel.Name,
                        Address = universityModel.Address,
                        Website = universityModel.Website,
                        PrimaryContactName = universityModel.PrimaryContactName,
                        Email = universityModel.Email,
                        PrimaryContactEmail = universityModel.PrimaryContactEmail
                    };
                    break;

                case UserType.Company:

                    var companyUserRepo = _genericUnitOfWork.GetRepository<CompanyUser, int>();
                    var companyUser = await companyUserRepo.FirstOrDefaultAsync(c => c.UserId == user.Id);

                    if (companyUser == null)
                        throw new ArgumentException("Company User doesnot exists");

                    var companyModel = await _companyService.GetAsync(companyUser.CompanyId, tenantId);

                   

                    response = new MyProfileViewModel()
                    {

                        Name = companyModel.Name,
                        Address = companyModel.Address,
                        Website = companyModel.Website,
                        PrimaryContactName = companyModel.PrimaryContactName,
                        Email = companyModel.Email,
                        PrimaryContactEmail = companyModel.PrimaryContactEmail
                    };
                    break;

                case UserType.Student:
                    response = await _studentService.GetProfileAsync(loggedInUserId, tenantId);
                    break;
            }
            return response;
        }
    }
}
