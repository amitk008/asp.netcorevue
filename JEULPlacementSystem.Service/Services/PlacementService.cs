﻿using AutoMapper;
using JEULPlacementSystem.Common.Configuration;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Domain;
using JEULPlacementSystem.Service.Interface;
using Kendo.DynamicLinqCore;
using System;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Service
{
    public class PlacementService : IPlacementService
    {
        private readonly IGenericUnitOfWork _genericUnitOfWork;
        private readonly IMapper _mapper;
        public PlacementService(IGenericUnitOfWork genericUnitOfWork, IMapper mapper)
        {
            _genericUnitOfWork = genericUnitOfWork;
            _mapper = mapper;
        }
        public async Task CreateAsync(PlacementViewModel model, Guid tenantId)
        {
            var repo = _genericUnitOfWork.GetRepository<Placement, string>();

            var placement = _mapper.Map<PlacementViewModel, Placement>(model);
            placement.Id = Guid.NewGuid();
            placement.CreatedById = CustomHttpContext.GetUserLoginInfo().UserId;
            placement.CreatedDate = GeneralService.CurrentDate;
            placement.TenantId = tenantId;

            repo.Add(placement);

            await _genericUnitOfWork.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id, Guid tenantId)
        {
            var placement = await _genericUnitOfWork.GetRepository<Placement, Guid>().FirstOrDefaultAsync(x => x.Id == id && x.TenantId == tenantId);

            if (placement == null)
                throw new ArgumentException("Placement does not exist");
            placement.IsActive = false;
            placement.IsDeleted = true;
            placement.DeletedDate = GeneralService.CurrentDate;

            await _genericUnitOfWork.SaveChangesAsync();

        }

        public DataSourceResult GetAll(KendoDataRequest kendoDataRequest, Guid tenantId)
        {
            var placements = _genericUnitOfWork.GetRepository<Placement, string>()
                                .GetAll(x => x.IsActive && !x.IsDeleted && x.TenantId == tenantId);


            return placements.ToDataSourceResult<Placement, PlacementViewModel>(_mapper, kendoDataRequest, new Sort() { Field = "CreatedDate", Dir = "desc" });
        }

        public async Task<PlacementViewModel> GetAsync(Guid id, Guid tenantId)
        {
            var Placement = await _genericUnitOfWork.GetRepository<Placement, Guid>().FirstOrDefaultAsync(x => x.Id == id && x.TenantId == tenantId);
            return _mapper.Map<Placement, PlacementViewModel>(Placement); ;
        }

        public async Task UpdateAsync(PlacementViewModel model, Guid tenantId)
        {
            var repo = _genericUnitOfWork.GetRepository<Placement, Guid>();
            var placement = await repo.FirstOrDefaultAsync(x => x.Id == model.Id && x.TenantId == tenantId);

            if (placement == null)
                throw new ArgumentException("Placement does not exist");



            var placementMapped = _mapper.Map(model, placement);

            placementMapped.ModifiedById = CustomHttpContext.GetUserLoginInfo().UserId;
            placementMapped.ModifiedDate = GeneralService.CurrentDate;

            repo.Update(placementMapped);
            await _genericUnitOfWork.SaveChangesAsync();
        }

         
    }
}
