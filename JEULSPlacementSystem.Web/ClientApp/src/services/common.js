import Vue from "vue";
import { vueinstance } from "../main.js";
import NProgress from "nprogress";

export default {
   
  isAuthenticated: function() {
    var hasToken = Vue.localStorage.get("token") !== null;

    return hasToken;
  },
  logOut: function() {
    Vue.localStorage.remove("userinfo");
    Vue.localStorage.remove("userpages");
    Vue.localStorage.remove("token");
    vueinstance.$router.push("/login");
  },
  getAuthToken: function() {
    return "Bearer " + Vue.localStorage.get("token");
  },
  
  getBaseUrl: function(url) {
    return process.env.VUE_APP_API_URL + "/" + url;
  },
  handleRejection: function(response) {
    if (response.status == 401) this.logOut();
    else {
      var message = "";
      if (response.body==="") 
        message = "Internal Sever Error";
        
      
      else if (Object.prototype.toString.call(response.body.ErrorMessages) === "[object Array]")
        message = response.body.ErrorMessages[0];

      else 
        message = response.body.error;
        vueinstance.$toast.error(message);
    }
  },
  

  postRequest(url, model) {
    return vueinstance.$http.post(url, model);
  },

  getById: function(url) {
    return vueinstance.$http.get(url);
  },

  getUserPages: function () {
    return this.jsonParse(Vue.localStorage.get('userpages'));
  },

  jsonParse: function (str) {
    if(str === null || str === 'undefined' || str.length === 0) {
      return null;
    }
    return JSON.parse(str);
  },

 
  getUserRole: function () {
    var userinfo =  this.jsonParse(Vue.localStorage.get('userinfo'));
    return userinfo.Role || "";
  },


}
