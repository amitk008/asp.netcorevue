import Vue from 'vue'
import Router from 'vue-router'

// Containers
const TheContainer = () => import('@/containers/TheContainer')

// Views
const Dashboard = () => import('@/views/dashboard/Dashboard')

//Account
const Login = () => import('@/views/account/Login')
const MyProfile = () => import('@/views/profile/MyProfile')
const ResetPassword = () => import('@/views/account/ResetPassword')
const RegisterStudent = () => import('@/views/account/RegisterStudent')

//Setup
const University = () => import('@/views/setup/University')
const Faculty = () => import('@/views/setup/Faculty')
const Degree = () => import('@/views/setup/Degree')
const Company = () => import('@/views/setup/Company')
const Placement = () => import('@/views/setup/Placement')
const Job = () => import('@/views/job/Job')
const JobApplications = () => import('@/views/job/Applications')
const StudentOnBoarding = () => import("../views/student/OnBoarding.vue");
const StudentList = () => import("../views/student/List.vue");
const StudentDetail = () => import("../views/student/Detail.vue");

//profile
const Education = () => import("../views/profile/student/education.vue");
const Experience = () => import("../views/profile/student/experience.vue"); 

//setting 
const EmailTemplate = () => import("../views/setting/EmailTemplate.vue");

// Views - Pages
const Page404 = () => import('@/views/pages/Page404')
const Page500 = () => import('@/views/pages/Page500')

//home
const JobList = () => import('@/views/home/jobs/List.vue')
const JobDetail = () => import('@/views/home/jobs/Detail.vue')

Vue.use(Router)

export default new Router({
  mode: '', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

function configRoutes() {
  return [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: TheContainer,
      children: [
        {
          path: 'dashboard', name: 'Dashboard', component: Dashboard, meta: { requiresAuth: true }
        },
      ]
    },
    {
      path: "/jobs",
      component: {
        render(c) { return c('router-view') }
      },
      children: [
        {
          path: "", name: "Jobs", component: JobList, meta: { requiresAuth: false }
        },
        {
          path: ":id", name: "", component: JobDetail, props: true, meta: { requiresAuth: false }
        }
      ]
    },
    {
      path: "/login",
      component: {
        render(c) { return c('router-view') }
      },
      children: [
        {
          path: "", name: "Login", component: Login, meta: { requiresAuth: false }
        }
      ]
    },
    {
      path: "/myprofile",
      component: TheContainer,
      children: [
        {
          path: "", name: "My Profile", component: MyProfile, name: "myprofile", meta: { requiresAuth: false }
        },
        {
          path: "education", component: Education, name: "education", props: true, meta: { requiresAuth: false }
        },
        {
          path: "experience", component: Experience, name: "experience", props: true, meta: { requiresAuth: false }
        },
      ]
    },

    {
      path: "/university",
      component: {
        render(c) {
          return c("router-view");
        }
      },
      meta: { label: "University" },
      children: [
        {
          path: "",
          component: TheContainer,
          children: [
            {
              path: "", name: "", component: University, meta: { requiresAuth: true }
            },

            {
              path: ":mode", component: University, props: true, meta: { requiresAuth: true }
            },
            {
              path: ":mode/:id", name: "", component: University, props: true, meta: { requiresAuth: true }
            }
          ]
        }
      ]
    },

    {
      path: "/company",
      component: {
        render(c) {
          return c("router-view");
        }
      },
      meta: { label: "Company" },
      children: [
        {
          path: "",
          component: TheContainer,
          children: [
            {
              path: "", name: "", component: Company, meta: { requiresAuth: true }
            },

            {
              path: ":mode", name: "", component: Company, props: true, meta: { requiresAuth: true }
            },
            {
              path: ":mode/:id", name: "", component: Company, props: true, meta: { requiresAuth: true }
            }
          ]
        }
      ]
    },

    {
      path: "/faculty",
      component: {
        render(c) {
          return c("router-view");
        }
      },
      meta: { label: "Faculty" },
      children: [
        {
          path: "",
          component: TheContainer,
          children: [
            {
              path: "",
              name: "",
              component: Faculty,
              meta: { requiresAuth: true }
            },

            {
              path: ":mode", //add,edit
              name: "",
              component: Faculty,
              props: true,
              meta: { requiresAuth: true }
            },
            {
              path: ":mode/:id",
              name: "",
              component: Faculty,
              props: true,
              meta: { requiresAuth: true }
            }
          ]
        }
      ]
    },

    {
      path: "/degree",
      component: {
        render(c) {
          return c("router-view");
        }
      },
      meta: { label: "Degree" },
      children: [
        {
          path: "",
          component: TheContainer,
          children: [
            {
              path: "",
              name: "",
              component: Degree,
              meta: { requiresAuth: true }
            },

            {
              path: ":mode", //add,edit
              name: "",
              component: Degree,
              props: true,
              meta: { requiresAuth: true }
            },
            {
              path: ":mode/:id",
              name: "",
              component: Degree,
              props: true,
              meta: { requiresAuth: true }
            }
          ]
        }
      ]
    },
    {
      path: "/placement",
      component: {
        render(c) {
          return c("router-view");
        }
      },
      meta: { label: "Placement" },
      children: [
        {
          path: "",
          component: TheContainer,
          children: [
            {
              path: "", name: "", component: Placement, meta: { requiresAuth: true }
            },

            {
              path: ":mode", name: "", component: Placement, props: true, meta: { requiresAuth: true }
            },
            {
              path: ":mode/:id", name: "", component: Placement, props: true, meta: { requiresAuth: true }
            }
          ]
        }
      ]
    },
    {
      path: "/job",
      component: {
        render(c) {
          return c("router-view");
        }
      },
      meta: { label: "Job" },
      children: [
        {
          path: "",
          component: TheContainer,
          children: [
            {
              path: "", name: "", component: Job, meta: { requiresAuth: true }
            },
            {
              path: "applications/:id", name: "", component: JobApplications, meta: { requiresAuth: true }
            },

            {
              path: ":mode", name: "AddEdit Job", component: Job, props: true, meta: { requiresAuth: true }
            },
            {
              path: ":mode/:id", name: "", component: Job, props: true, meta: { requiresAuth: true }
            }
          ]
        }
      ]
    },

    {
      path: "/student",

      component: {
        render(c) {
          return c("router-view");
        }
      },
      name: "Student",
      children: [
        {
          path: "",
          component: TheContainer,

          children: [
            { path: "onboarding", name: "Student OnBoardingn", component: StudentOnBoarding, meta: { requiresAuth: true } },
            { path: "list", name: "Student List", component: StudentList, meta: { requiresAuth: true } },
            { path: "detail/:id", name: "Student Detail", component: StudentDetail, meta: { requiresAuth: true } },

          ]
        }
      ]
    },
    {
      path: "/setting",
      component: {
        render(c) {
          return c("router-view");
        }
      },
      meta: { label: "Setting" },
      children: [
        {
          path: "",
          component: TheContainer,
          children: [
            {
              path: "emailtemplate", name: "", component: EmailTemplate, meta: { requiresAuth: true }
            },

          ]
        }
      ]
    },
    {
      path: "/register",
      component: {
        render(c) { return c("router-view"); }
      },
      children: [
        {
          path: "", name: "Student Registration", component: RegisterStudent, meta: { requiresAuth: false }
        },
      ]
    },

    {
      path: "/resetpassword",
      component: {
        render(c) { return c("router-view"); }
      },
      children: [
        {
          path: "", name: "ResetPassword", component: ResetPassword, meta: { requiresAuth: false }
        },
      ]
    },


    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render(c) { return c('router-view') }
      },
      children: [
        {
          path: '404', name: 'Page404', component: Page404
        },
        {
          path: '500', name: 'Page500', component: Page500
        },

      ]
    }
  ]
}

