import Vue from 'vue' 
import moment from "moment";

 
Vue.filter('striphtml', function (value) {
  var div = document.createElement("div");
  div.innerHTML = value;
  var text = div.textContent || div.innerText || "";
  return text;
});

Vue.filter('customDateFormat', function (date) {
  
    if (date) {
       return moment(date, "YYYY-MM-DD").format("MMMM D, YYYY");
    }
 
});
 
