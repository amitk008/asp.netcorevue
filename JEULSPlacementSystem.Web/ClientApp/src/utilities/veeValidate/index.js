import Vue from 'vue'
import VeeValidate from 'vee-validate'

const dictionary = {
  en: {
    messages: {
      min: (field, params) => ` ${field} must be at least ${params} characters.`,
      max: (field, params) => ` ${field} may not be greater than ${params} characters. `,
      between: (field, params) => `${field} must be between ${params[0]} and ${params[1]}`,
      required: (field, params) => `${field} is required`,
      numeric: (field, params) => `${field} must be a positive number.`,
      digits: (field, params) => `length must be ${params[0]}`,
      confirmed: (field, params) => `${field} confirmation does not match`,
      after: (field, params) => `${field} must be greater than ${params[0]} `,
    },

    custom: {
      field: {
        required: 'field is required',
        digits: (field, params) => `length must be ${params[0]}`
      }
    },
    attributes: {
      email: 'Email Address',
      username: 'Username',
      password: 'Password',
      confirmPassword: 'Confirm Password'
    }
  }
};

const config = {
  dictionary: dictionary,
  errorBagName: 'errors', // change if property conflicts
  fieldsBagName: 'fields',
  delay: 0,
  locale: 'en',
  strict: true,
  classes: true,
  classNames: {
    touched: 'touched', // the control has been blurred
    untouched: 'untouched', // the control hasn't been blurred
    valid: 'valid', // model is valid
    invalid: 'invalid', // model is invalid
    pristine: 'pristine', // control has not been interacted with
    dirty: 'dirty' // control has been interacted with
  },
  events: 'input|blur',
  inject: true,
  validity: false,
  aria: true
}

VeeValidate.Validator.extend('age', {
  getMessage: field => `Age should be atleast 18 years.Please enter a valid Date of Birth.`,
  validate: value => {
    var today = new Date();
    var birthDate = new Date(value);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    if (age >= 18) {
      return true
    }
    return false;
  }

});
 

VeeValidate.Validator.extend('validatePassword', {
  getMessage: field => ` ${field} must be at least 8 characters long, and contain a number,one uppercase,one lower and one special characters.`,
  validate: value => {
    if (value != null && value.trim() != '') {
      var REGEXP = /(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*\W)[a-zA-Z0-9`!@#$%^&*()_=+\-\{\}\[\]|\\":;,./?]{8,25}/;
      return REGEXP.test(value) ? true : false;
    }
    return true;
  }
});
 


Vue.use(VeeValidate, config)
 