import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from './assets/icons/icons.js'
import store from './store'
import CommonService from './services/common'
import CKEditor from 'ckeditor4-vue';
import { BootstrapVue } from 'bootstrap-vue'

Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.use( CKEditor );
Vue.use(BootstrapVue)
Vue.prototype.$log = console.log.bind(console)

console.log(process.env.VUE_APP_API_URL)

router.beforeEach((to, from, next) => {
  var isAuthRequire = to.matched.some(record => record.meta.requiresAuth);
  if (isAuthRequire && to.meta != undefined && to.meta.requiresAuth != undefined) {
    isAuthRequire = to.meta.requiresAuth
  }

  if (isAuthRequire) {
    if (!CommonService.isAuthenticated()) {
      next({
        path: '/login'
      })
    }

    else {
      var isValid = {
        path: '/pages/404'
      }
      var pages = CommonService.getUserPages();
      var hasPermissionToView = pages.some(function (page, i) {
        return to.fullPath.indexOf(page.to) > -1;
      });


      if (hasPermissionToView) isValid = true;
    }

    next(isValid)
  }
  next();
})
export const vueinstance = new Vue({
  el: '#app',
  router,
  store,
  icons,
  template: '<App/>',
  components: {
    App
  },
  methods: {

  },
})

