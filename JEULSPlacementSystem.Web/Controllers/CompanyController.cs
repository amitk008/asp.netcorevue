﻿using System;
using System.Net;
using System.Threading.Tasks;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Model.Common;
using JEULPlacementSystem.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JEULPlacementSystem.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CompanyController : BaseApiController
    {
        private readonly ICompanyService _companyService;

        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CompanyViewModel Company)
        {
             
                await _companyService.CreateAsync(Company, TenantId());

                return new OkObjectResult(new Response
                {
                    Message = "Company Added Successfully!",
                    StatusCode = HttpStatusCode.OK
                });
             
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Update([FromBody] CompanyViewModel Company, Guid id)
        {
            
                Company.Id = id;
                await _companyService.UpdateAsync(Company, TenantId());

                return new OkObjectResult(new Response
                {
                    Message = "Company Updated Successfully!",
                    StatusCode = HttpStatusCode.OK
                });
             

        }

        [Route("getall")]
        [HttpPost]
        public async Task<IActionResult> Get( KendoDataRequest kendoDataRequest)
        {
           
                var universities = _companyService.GetAll(kendoDataRequest, TenantId());
                return Ok(universities);
             
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> Get( Guid id)
        {
              var Company = await _companyService.GetAsync(id, TenantId());


                return new OkObjectResult(new Response<CompanyViewModel>
                {
                    Content = Company,
                    Message = "Universoty Retrive Successfully!",
                    StatusCode = HttpStatusCode.OK
                });
             
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
             await _companyService.DeleteAsync(id, TenantId());

                return new OkObjectResult(new Response<CompanyViewModel>
                { 
                    Message = "Company Deleted Successfully!",
                    StatusCode = HttpStatusCode.OK
                });
             
        }
    }
}
