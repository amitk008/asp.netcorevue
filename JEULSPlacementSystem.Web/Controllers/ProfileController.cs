﻿using System;
using System.Net;
using System.Threading.Tasks;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Controllers;
using JEULPlacementSystem.Model.Common;
using JEULPlacementSystem.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JEUL.Placement.Systemt.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProfileController : BaseApiController
    {
        private readonly IProfileService _profileService;

        public ProfileController(IProfileService profileService)
        {
            _profileService = profileService;
        }


        [Route("update")]
        [HttpPost]
        public async Task<IActionResult> UpdateProfie(MyProfileViewModel model)
        {
            return Ok(await _profileService.UpdateProfile(model, TenantId()));
        }


        [HttpGet("get")]
        public async Task<IActionResult> GetDetails()
        {
            var response = await _profileService.GetAsync(TenantId());


            return new OkObjectResult(new Response<MyProfileViewModel>
            {
                Content = response,
                Message = "Data Retrive Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }




    }
}
