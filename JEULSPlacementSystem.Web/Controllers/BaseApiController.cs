﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JEULPlacementSystem.Common.Configuration;
using Microsoft.AspNetCore.Mvc;

namespace JEULPlacementSystem.Controllers
{
    public class BaseApiController : ControllerBase
    {
        protected Guid CurrentUserId()
        {
            return CustomHttpContext.GetUserLoginInfo().UserId;
        }

        protected Guid TenantId()
        {
            return CustomHttpContext.GetUserLoginInfo().TenantId;
        }
    }
}
