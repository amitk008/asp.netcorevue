﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Model.Common;
using JEULPlacementSystem.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JEULPlacementSystem.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class JobController : BaseApiController
    {
        private readonly IJobService _jobService;

        public JobController(IJobService jobService)
        {
            _jobService = jobService;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] JobViewModel Job)
        {

            await _jobService.CreateAsync(Job, TenantId());

            return new OkObjectResult(new Response
            {
                Message = "Job Added Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Update([FromBody] JobViewModel Job, Guid id)
        {

            Job.Id = id;
            await _jobService.UpdateAsync(Job, TenantId());

            return new OkObjectResult(new Response
            {
                Message = "Job Updated Successfully!",
                StatusCode = HttpStatusCode.OK
            });


        }

        [AllowAnonymous]
        [Route("getall")]
        [HttpPost]
        public async Task<IActionResult> GetAll(KendoDataRequest kendoDataRequest)
        {

            var result = _jobService.GetAll(kendoDataRequest,null);
            return Ok(result);

        }

        [Route("getallbytenant")]
        [HttpPost]
        public IActionResult  GetAllByTenant(KendoDataRequest kendoDataRequest)
        {

            var result = _jobService.GetAll(kendoDataRequest, TenantId());
            return Ok(result);

        }
        [AllowAnonymous]
        [HttpGet("{id:guid}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var Job = await _jobService.GetAsync(id);


            return new OkObjectResult(new Response<JobViewModel>
            {
                Content = Job,
                Message = "Universoty Retrive Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _jobService.DeleteAsync(id, TenantId());

            return new OkObjectResult(new Response<JobViewModel>
            {
                Message = "Job Deleted Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [Route("{id}/applications/{name}")]
        [HttpGet]
        public IActionResult GetStudentListByJob(Guid id, string name="")
        {
            name = null;
            var students = _jobService.GetStudentListByJob(id,name, TenantId());


            return new OkObjectResult(new Response<List<StudentListViewModel>>
            {
                Content = students,
                Message = "Students Retrive Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [Route("{id}/sortlist")]
        [HttpPost]
        public async Task<IActionResult> SortListStudents(Guid id, JobSortListViewModel model)
        {

            await _jobService.SortListStudents(id, model,TenantId());

            return new OkObjectResult(new Response
            {
                Message = "Students SortListed Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

    }
}
