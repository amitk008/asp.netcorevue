﻿using System;
using System.Net;
using System.Threading.Tasks;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Model.Common;
using JEULPlacementSystem.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JEULPlacementSystem.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class UniversityController : ControllerBase
    {
        private readonly IUniversityService _universityService;

        public UniversityController(IUniversityService universityService)
        {
            _universityService = universityService;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] UniversityViewModel university)
        {

            await _universityService.CreateAsync(university);

            return new OkObjectResult(new Response
            {
                Message = "University Added Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Update([FromBody] UniversityViewModel university, Guid id)
        {

            university.Id = id;
            await _universityService.UpdateAsync(university);

            return new OkObjectResult(new Response
            {
                Message = "University Updated Successfully!",
                StatusCode = HttpStatusCode.OK
            });


        }

        [Route("getall")]
        [HttpPost]
        public async Task<IActionResult> Get(KendoDataRequest kendoDataRequest)
        {

            var universities = _universityService.GetAll(kendoDataRequest);
            return Ok(universities);


        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var university = await _universityService.GetAsync(id);


            return new OkObjectResult(new Response<UniversityViewModel>
            {
                Content = university,
                Message = "Universoty Retrive Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _universityService.DeleteAsync(id);

            return new OkObjectResult(new Response<UniversityViewModel>
            {
                //Content = "",
                Message = "University Deleted Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [Route("{id}/approve")]
        [HttpPut]
        public async Task<IActionResult> Approve(Guid id)
        {
            var response = await _universityService.Approve(id);

            return new OkObjectResult(new Response<UniversityViewModel>
            {
                Message = $"University {(response.IsApproved ? "Approved" : "Un-Approved")} Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [Route("{id}/active")]
        [HttpPut]
        public async Task<IActionResult> ActivateDeactivateUniversity(Guid id)
        {
            var response = await _universityService.ActivateDeactivateUniversity(id);

            return new OkObjectResult(new Response<UniversityViewModel>
            {
                Message = $"University {(response.IsActive ? "Activated" : "Deactivated")} Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }
    }
}
