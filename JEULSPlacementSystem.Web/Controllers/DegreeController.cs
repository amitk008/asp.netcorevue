﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Model.Common;
using JEULPlacementSystem.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JEULPlacementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DegreeController : BaseApiController
    {
        private readonly IDegreeService _degreeService;

        public DegreeController(IDegreeService degreeService)
        {
            _degreeService = degreeService;
        }


        [HttpPost]
        public async Task<IActionResult> Create([FromBody] DegreeViewModel degree)
        {

            await _degreeService.CreateAsync(degree, TenantId());

            return new OkObjectResult(new Response
            {
                Message = "Degree Added Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Update([FromBody] DegreeViewModel degree, Guid id)
        {
            degree.Id = id;
            await _degreeService.UpdateAsync(degree, TenantId());

            return new OkObjectResult(new Response
            {
                //Content = "",
                Message = "Degree Updated Successfully!",
                StatusCode = HttpStatusCode.OK
            });


        }

        [Route("getall")]
        [HttpPost]
        public async Task<IActionResult> Get(KendoDataRequest kendoDataRequest)
        {

            var faculties = _degreeService.GetAll(kendoDataRequest, TenantId());

            return Ok(faculties);

        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> Get(Guid id)
        {

            var degree = await _degreeService.GetAsync(id, TenantId());


            return new OkObjectResult(new Response<DegreeViewModel>
            {
                Content = degree,
                Message = "Degree Retrive Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {

            await _degreeService.DeleteAsync(id, TenantId());

            return new OkObjectResult(new Response<UniversityViewModel>
            {
                Message = "Degree Deleted Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [HttpGet("list")]
        public IActionResult ListDegrees()
        {

            var response = _degreeService.ListDegrees( TenantId());

            return new OkObjectResult(new Response<IList<ListViewModel>>
            {
                Content = response,
                Message = "Degree Retrive Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }
    }
}
