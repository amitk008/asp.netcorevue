﻿using System.Threading.Tasks;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Service.Interface;
using Microsoft.AspNetCore.Mvc;

namespace JEULPlacementSystem.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [Route("login")]
        [HttpPost]
        public async Task<IActionResult> Login(SignInViewModel model)
        {
            return Ok(await _accountService.Login(model));
        }

        [Route("resetpassword")]
        [HttpPost]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            return Ok(await _accountService.ResetPassword(model));
        }
    }
}
