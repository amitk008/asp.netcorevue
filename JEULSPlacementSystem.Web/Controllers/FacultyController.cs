﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Controllers;
using JEULPlacementSystem.Model.Common;
using JEULPlacementSystem.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JEUL.Placement.Systemt.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FacultyController : BaseApiController
    {
        private readonly IFacultyService _facultyService;

        public FacultyController(IFacultyService facultyService)
        {
            _facultyService = facultyService;
        }


        [HttpPost]
        public async Task<IActionResult> Create([FromBody] FacultyViewModel faculty)
        {
            await _facultyService.CreateAsync(faculty, TenantId());

            return new OkObjectResult(new Response
            {
                Message = "Faculty Added Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Update([FromBody] FacultyViewModel faculty, Guid id)
        {
            faculty.Id = id;
            await _facultyService.UpdateAsync(faculty, TenantId());

            return new OkObjectResult(new Response
            {
                Message = "Faculty Updated Successfully!",
                StatusCode = HttpStatusCode.OK
            });


        }

        [Route("getall")]
        [HttpPost]
        public async Task<IActionResult> Get(KendoDataRequest kendoDataRequest)
        {
            var faculties = _facultyService.GetAll(kendoDataRequest, TenantId());

            return Ok(faculties);

        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var faculty = await _facultyService.GetAsync(id, TenantId());


            return new OkObjectResult(new Response<FacultyViewModel>
            {
                Content = faculty,
                Message = "Faculty Retrive Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _facultyService.DeleteAsync(id, TenantId());

            return new OkObjectResult(new Response<UniversityViewModel>
            {
                Message = "Faculty Deleted Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [HttpGet("list")]
        public IActionResult ListFaculty()
        {

            var response = _facultyService.ListFaculty(TenantId());

            return new OkObjectResult(new Response<IList<ListViewModel>>
            {
                Content = response,
                Message = "Faculty Retrive Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }
    }
}
