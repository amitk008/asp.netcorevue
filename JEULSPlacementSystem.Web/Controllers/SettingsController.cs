﻿using System;
using System.Net;
using System.Threading.Tasks;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Model.Common;
using JEULPlacementSystem.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JEULPlacementSystem.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class SettingsController : BaseApiController
    {
        private readonly ISettingsService _settingsService;

        public SettingsController(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        [HttpPost("emailtemplate")]
        public async Task<IActionResult> AddEditEmailTemplate([FromBody] EmailTemplateViewModel model)
        {

            await _settingsService.AddEditEmailTemplate(model, TenantId());

            return new OkObjectResult(new Response
            {
                Message = "Settings Saved Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }
 

        [HttpGet("emailtemplate")]
        public async Task<IActionResult> Get()
        {
            var result = await _settingsService.GetEmailTemplateAsync(TenantId());


            return new OkObjectResult(new Response<EmailTemplateViewModel>
            {
                Content = result,
                Message = "Settings Retrive Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

       
    }
}
