﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Controllers;
using JEULPlacementSystem.Model.Common;
using JEULPlacementSystem.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JEUL.Placement.Systemt.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PlacementController : BaseApiController
    {
        private readonly IPlacementService _placementService;

        public PlacementController(IPlacementService placementService)
        {
            _placementService = placementService;
        }


        [HttpPost]
        public async Task<IActionResult> Create([FromBody] PlacementViewModel Placement)
        {
            await _placementService.CreateAsync(Placement, TenantId());

            return new OkObjectResult(new Response
            {
                Message = "Placement Added Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Update([FromBody] PlacementViewModel Placement, Guid id)
        {
            Placement.Id = id;
            await _placementService.UpdateAsync(Placement, TenantId());

            return new OkObjectResult(new Response
            {
                Message = "Placement Updated Successfully!",
                StatusCode = HttpStatusCode.OK
            });


        }

        [Route("getall")]
        [HttpPost]
        public async Task<IActionResult> Get(KendoDataRequest kendoDataRequest)
        {
            var faculties = _placementService.GetAll(kendoDataRequest, TenantId());

            return Ok(faculties);

        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var Placement = await _placementService.GetAsync(id, TenantId());


            return new OkObjectResult(new Response<PlacementViewModel>
            {
                Content = Placement,
                Message = "Placement Retrive Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _placementService.DeleteAsync(id, TenantId());

            return new OkObjectResult(new Response<UniversityViewModel>
            {
                Message = "Placement Deleted Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

    }
}
