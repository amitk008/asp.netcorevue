﻿using System;
using System.Net;
using System.Threading.Tasks;
using JEULPlacementSystem.Common.Configuration;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Common.Extensions;
using JEULPlacementSystem.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using JEULPlacementSystem.Common.Helpers;
using System.IO;
using JEULPlacementSystem.Model.Common;
using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;
using Rotativa.AspNetCore;

namespace JEULPlacementSystem.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class StudentController : BaseApiController
    {
        private IWebHostEnvironment _hostingEnvironment;
        private readonly IStudentService _studentService;

        public StudentController(IStudentService studentService, IWebHostEnvironment environment)
        {
            _hostingEnvironment = environment;
            _studentService = studentService;
        }


        [Route("onboarding")]
        [HttpPost]
        public async Task<IActionResult> StudentOnBoarding()
        {
            int fileCount = CustomHttpContext.Current.Request.Form.Files.Count;
            IFormFile postedFile = fileCount > 0 ? CustomHttpContext.Current.Request.Form.Files["File"] : null;

            if (postedFile == null)
                throw new ArgumentNullException($"File not found", innerException: null);

            var uploadDataModel = Extensions.SaveFile(postedFile, _hostingEnvironment.WebRootPath);
            DataTable dt = Extensions.ConvertCSVtoDataTable(uploadDataModel.PhysicalPath);

            var ismatch = DatatableObjectComparer<StudentOnBoardingViewModel>.ValidateDatatable(dt);
            if (!ismatch)
                throw new ArgumentException("Invalid File Template");
            var responseData = dt.ConvertDataTableToGenericList<StudentOnBoardingViewModel>();

            return Ok(await _studentService.OnBoardingRegistration(responseData, TenantId()));

        }

        [HttpGet]
        [Route("downloadsample")]
        public async Task<IActionResult> DownloadSampleFile()
        {
            var currentDirectory = Path.Combine(_hostingEnvironment.WebRootPath, "Resources", "CsvFile");
            if (!Directory.Exists(currentDirectory))
            {
                Directory.CreateDirectory(currentDirectory);
            }
            var filepath = Path.Combine(Path.Combine(currentDirectory), "sample.csv");
            if (!System.IO.File.Exists(filepath))
            {
                throw new ArgumentException("FILE DOESNOT EXISTS!");
            }
            var filename = Path.GetFileName(filepath);

            var memory = new MemoryStream();
            using (var stream = new FileStream(filepath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }

            memory.Position = 0;
            return File(memory, "text/csv", filename);

        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Registration([FromBody] StudentViewModel model)
        {

            await _studentService.Registration(model);

            return new OkObjectResult(new Response
            {
                Message = "Student Registered Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [Route("getall")]
        [HttpPost]
        public IActionResult Get(KendoDataRequest kendoDataRequest)
        {

            var universities = _studentService.GetAll(kendoDataRequest, TenantId());
            return Ok(universities);

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var student = await _studentService.GetAsync(id, TenantId());

            return new OkObjectResult(new Response<StudentViewModel>
            {
                Content = student,
                Message = "Student Details Retrive Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [HttpGet("{id}/details")]
        public async Task<IActionResult> Details(int id)
        {
            var student = await _studentService.GetDetails(id, TenantId());

                return new OkObjectResult(new Response<MyProfileViewModel>
            {
                Content = student,
                Message = "Student Details Retrive Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [HttpGet("{id}/downloadresume")]
        public async Task<IActionResult> DownloadResume(int id)
        {
            var response =  await _studentService.DownloadResume(id, TenantId());

            var pdf = new ViewAsPdf("CVResume",response.Item1)
            {
                FileName = "Resume.pdf"
            };
           
            byte[] applicationPDFData = pdf.BuildFile(this.ControllerContext).Result;
            return File(applicationPDFData,
                   System.Net.Mime.MediaTypeNames.Application.Pdf, $"{response}_{DateTime.UtcNow.ToString("yyyy-MM-dd")}");
        }


        [HttpGet("job/list")]
        public async Task<IActionResult> JobList(string title)
        {
            var jobList = await _studentService.GetAllJobList(title, TenantId());


            return new OkObjectResult(new Response<List<StudentJobListViewModel>>
            {
                Content = jobList,
                Message = "Job List Retrive Successfully!",
                StatusCode = HttpStatusCode.OK
            });

        }

        [Route("job/apply")]
        [HttpPost]
        public async Task<IActionResult> ApplyJob(JobCounterViewModel model)
        {

            await _studentService.ApplyJob(model);

            return new OkObjectResult(new Response<string>
            {
                Message = "Job Applied Successfully!",
                StatusCode = HttpStatusCode.OK
            });


        }


        [Route("job/save")]
        [HttpPost]
        public async Task<IActionResult> SaveJob(JobCounterViewModel model)
        {

            var response = await _studentService.SaveJobAsync(model);

            return new OkObjectResult(new Response<string>
            {
                Message = !response ? "Job UnSaved" : "Job Saved Successfully!",
                StatusCode = HttpStatusCode.OK
            });


        }


        [Route("job/noofviewscount")]
        [HttpPost]
        public async Task<IActionResult> ViewCount(JobCounterViewModel model)
        {

            await _studentService.UpdateViewCountAsync(model);
            return Ok();

        }

    }
}
