﻿using System;
using System.Collections.Generic; 
using System.Net;
using JEULPlacementSystem.Common.Exceptions;
using JEULPlacementSystem.Model.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace JEULPlacementSystem.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception is ValidationException)
            {
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Result =
                new JsonResult(new Response<string>()
                {
                    Success = false,
                    ErrorMessages = ((ValidationException)context.Exception).Failures

                });

                return;
            }

             

            var code = HttpStatusCode.InternalServerError;

            context.HttpContext.Response.ContentType = "application/json";
            context.HttpContext.Response.StatusCode = (int)code;
            context.Result = new JsonResult(new Response<string>()
            {
                Success = false,
                ErrorMessages = new List<string>() { context.Exception.Message }

            });
        }
    }
}
