using JEULPlacementSystem.Common.Configuration;
using JEULPlacementSystem.Common.ViewModel;
using JEULPlacementSystem.Data;
using JEULPlacementSystem.Filters;
using JEULPlacementSystem.Service;
using JEULPlacementSystem.Service.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Reflection;
using System.Text;
using AutoMapper;
using FluentValidation.AspNetCore;
using JEULPlacementSystem.Common.Validators;
using JEULPlacementSystem.Service.Infrastructure;
using JEULPlacementSystem.Common.ViewModel.AppSettings;
using JEULPlacementSystem.Service.Services;
using JEULPlacementSystem.Domain;
using VueCliMiddleware;
using System.Text.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using DinkToPdf.Contracts;
using DinkToPdf;
using Rotativa.AspNetCore;

namespace JEULSPlacementSystem.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                 builder => builder.WithOrigins("http://localhost:8080", "http://jeulsys-env-dev.ap-southeast-1.elasticbeanstalk.com")
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });
       

            services.AddAutoMapper(typeof(UniversityMapper));
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
            // For Entity Framework  

            services.AddIdentity<User, Role>(opt =>
            {
                opt.User.RequireUniqueEmail = true;
                opt.SignIn.RequireConfirmedEmail = true;
            })
               .AddEntityFrameworkStores<JulsDbContext>()
               .AddDefaultTokenProviders();



            var connectionString = Configuration.GetConnectionString("ConnStr");
            var migrationsAssembly = typeof(JulsDbContext).GetTypeInfo().Assembly.GetName().Name;

            services.AddDbContext<JulsDbContext>(options =>
                options.UseSqlServer(connectionString, b => b.MigrationsAssembly(migrationsAssembly)), ServiceLifetime.Transient);
            services.Configure<EncryptionKeys>(Configuration.GetSection("EncryptionKeys"));
            services.Configure<EmailConfiguration>(Configuration.GetSection("EmailConfiguration"));
            services.Configure<JEULSUrl>(Configuration.GetSection("JEULSUrl"));
            //services.AddSwaggerGen();


            services.AddTransient<IValidatorInterceptor, CustomInterceptor>();
            services.AddTransient<IDatabaseInitializer, DatabaseInitializer>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IJwtFactory, JwtFactory>();
            services.AddScoped<JwtIssuerOptions, JwtIssuerOptions>();
            services.AddScoped(typeof(IGenericRepository<,>), typeof(GenericRepository<,>));
            services.AddScoped<IGenericUnitOfWork, GenericUnitOfWork>();
            services.AddScoped<EncryptionService, EncryptionService>();
            services.AddScoped<IEmailService, EmailService>();

            services.AddScoped<IUniversityService, UniversityService>();
            services.AddScoped<ICompanyService, CompanyService>();
            services.AddScoped<IFacultyService, FacultyService>();
            services.AddScoped<IDegreeService, DegreeService>();
            services.AddScoped<IPlacementService, PlacementService>();
            services.AddScoped<IProfileService, ProfileService>();
            services.AddScoped<IJobService, JobService>();
            services.AddScoped<IStudentService, StudentService>();
            services.AddScoped<ISettingsService, SettingsService>();
            services.AddTransient<ViewRender, ViewRender>();
            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));

            services
            .AddMvc(options =>
                    {
                        options.Filters.Add(typeof(CustomExceptionFilterAttribute));
                    }).AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<UniversityValidator>());

            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();

                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                options.SerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
            });
            // jwt wire up
            // Get options from app settings
            var jwtAppSettingOptions = Configuration.GetSection
                (nameof(JwtIssuerOptions));
            var secretKey = jwtAppSettingOptions[nameof(JwtIssuerOptions.SecretKey)];
            var _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

                ValidateAudience = true,
                ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,

                RequireExpirationTime = false,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };


            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(configureOptions =>
            {
                configureOptions.ClaimsIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                configureOptions.TokenValidationParameters = tokenValidationParameters;
                configureOptions.SaveToken = true;
            });

            // Configure JwtIssuerOptions
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });
            services.AddHttpContextAccessor();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseCors("CorsPolicy");

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            CustomHttpContext.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            RotativaConfiguration.Setup(env.WebRootPath);

            app.UseSpa(spa =>
            {
                if (env.IsDevelopment())
                    spa.Options.SourcePath = "ClientApp";
                else
                    spa.Options.SourcePath = "dist";

                if (env.IsDevelopment())
                {
                    spa.UseVueCli(npmScript: "serve");
                }

            });
        }
    }
}
