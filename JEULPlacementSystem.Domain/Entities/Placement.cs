﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JEULPlacementSystem.Domain
{
    public class Placement : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public Guid TenantId { get; set; }
        
    }
}
