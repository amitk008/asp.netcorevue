﻿using System;

namespace JEULPlacementSystem.Domain
{
    public class Degree : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public Guid FacultyId { get; set; }
        public virtual Faculty Faculty { get; set; }
        public Guid TenantId { get; set; }
        
    }
}
