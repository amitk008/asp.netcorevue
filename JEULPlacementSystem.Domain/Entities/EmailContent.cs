﻿using System;

namespace JEULPlacementSystem.Domain
{
    public class EmailContent : BaseEntity<Guid>
    {
        public Guid CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public string SelectedEmailContent { get; set; }
        public string NotSelectedEmailContent { get; set; }
        public Guid TenantId { get; set; }

    }

    
}
