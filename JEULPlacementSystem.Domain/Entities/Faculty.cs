﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JEULPlacementSystem.Domain
{
    public class Faculty : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public Guid TenantId { get; set; }
        
    }
}
