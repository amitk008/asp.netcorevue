﻿using System;
using System.Collections.Generic;

namespace JEULPlacementSystem.Domain
{
    public class Company : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }
        public string Size { get; set; }
        public Guid TenantId { get; set; }
        public virtual ICollection<CompanyUser> CompanyUsers { get; set; } = new HashSet<CompanyUser>();
    
    }

    public class CompanyUser
    {
        

        public Guid CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public Guid UserId { get; set; }
        public virtual User User { get; set; }

        public bool IsActive { get; set; } = true;
        public Guid TenantId { get; set; }




    }
}
