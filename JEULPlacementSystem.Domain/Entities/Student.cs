﻿using System;
using System.Collections.Generic;
using JEULPlacementSystem.Domain.Enum;

namespace JEULPlacementSystem.Domain
{
    public class StudentTempRegistration
    {
        public Guid Id { get; set; }
        public string Email { get; set; }

        public Guid UniversityId { get; set; }
        public virtual University University { get; set; }

        public StudentRegistrationStatus Status { get; set; }
        public Guid TenantId { get; set; }
       
    }

    public class Student : BaseEntity<int>
    {
        
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public Guid UniversityId { get; set; }
        public virtual University University { get; set; } 
        public Guid TenantId { get; set; }
        
        public virtual StudentProfile StudentProfile { get; set; }
        public virtual ICollection<StudentEducationDetail> StudentEducationDetails { get; set; } = new HashSet<StudentEducationDetail>();
        public virtual ICollection<StudentExperienceDetail> StudentExperienceDetails { get; set; } = new HashSet<StudentExperienceDetail>();
        public virtual ICollection<JobApplied> JobApplieds { get; set; } = new HashSet<JobApplied>();
        public virtual ICollection<JobSortListed> JobSortLists { get; set; } = new HashSet<JobSortListed>();

    }

    public class StudentProfile
    {
        public int StudentId { get; set; }
        public virtual Student Student { get; set; }

        public string Gender { get; set; }
        public string MobileNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public MartialStatus MartialStatusId { get; set; }
        public string Nationality { get; set; }
        public JobType JobTypeId { get; set; }
        public string CurrentSalary { get; set; }
        public string ExpectSalary { get; set; }
   
        public string Skills { get; set; }
        public string Objective { get; set; }

        public string PermanentAddress { get; set; }
        public string CurrentAddress { get; set; }

    }

    public class StudentEducationDetail
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public virtual Student Student { get; set; }
        public string Degree { get; set; }
        public string Faculty { get; set; }
        public string Institute { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public GradingType GradingTypeId { get; set; }
        public double  Marks { get; set; }
        public bool IsActive { get; set; } = true;
    }

    public class StudentExperienceDetail
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public virtual Student Student { get; set; }
        public string CompanyName { get; set; }
        public string JobTitle { get; set; }
        public JobType JobTypeId { get; set; }

        public bool IsCurrentJob { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; } 
        public string Responsibilities { get; set; }
        public bool IsActive { get; set; } = true;

    }
}
