﻿using JEULPlacementSystem.Domain.Enum;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace JEULPlacementSystem.Domain
{
    public class User: IdentityUser<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        
        

        public bool SystemDefined { get; set; }
        public bool IsActive { get; set; } = true;

        public UserType UserType { get; set; }

   
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? LastLoginDateTime { get; set; }
        public DateTime? LastPasswordChangeDate { get; set; }

        public virtual ICollection<UniversityUser> UniversityUsers { get; set; } = new HashSet<UniversityUser>();
        public virtual ICollection<CompanyUser> CompanyUsers { get; set; } = new HashSet<CompanyUser>();
        public virtual ICollection<Student> Students { get; set; } = new HashSet<Student>();


        //public Guid? CreatedById { get; set; }
        //public virtual User CreatedBy { get; set; }
        //public Guid? ModifiedById { get; set; }
        //public virtual User ModifiedBy { get; set; }
        //public virtual ICollection<UserRole> UserRoles { get; set; }

    }
}
