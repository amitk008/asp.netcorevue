﻿using System;
using System.Collections.Generic;

namespace JEULPlacementSystem.Domain
{
    public class University: BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }
        public virtual ICollection<UniversityUser> UniversityUsers { get; set; } = new HashSet<UniversityUser>();
    }

    public class UniversityUser 
    {
       

        public Guid UniversityId { get; set; }
        public virtual University University { get; set; }

        public Guid UserId { get; set; }
        public virtual User User { get; set; }

        public bool IsActive { get; set; } = true;


    }

}
