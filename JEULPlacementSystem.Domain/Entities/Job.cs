﻿using System;
using System.Collections.Generic;

namespace JEULPlacementSystem.Domain
{
    public class Job : BaseEntity<Guid>
    {
        public string Title { get; set; }
        public Guid CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public DateTime DatePublished { get; set; }
        public DateTime EndDate { get; set; }
        public string Salary { get; set; }

        public string Requirements { get; set; }
        public Guid DegreeId { get; set; }
        public virtual Degree Degree { get; set; }
        public int EligibilityAge { get; set; }
        public int NoOfVacancy { get; set; }
        public string EligibilityNationality { get; set; }
        public string JobDescription { get; set; }

        public string JobType { get; set; }
        public Guid TenantId { get; set; }
        public virtual ICollection<JobViewCount> JobViewCounts { get; set; }
        public virtual ICollection<JobApplied> JobsApplied { get; set; }
        public virtual ICollection<JobSortListed> JobSortLists { get; set; }

    }

    public class JobViewCount  
    {
        public Guid JobId { get; set; }
        public virtual Job Job { get; set; }
        public int StudentId { get; set; }
        public virtual Student Student { get; set; }
    }

    public class JobSaved
    {
        public Guid JobId { get; set; }
        public virtual Job Job { get; set; }
        public int StudentId { get; set; }
        public virtual Student Student { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class JobApplied
    {
        public Guid JobId { get; set; }
        public virtual Job Job { get; set; }

        public int StudentId { get; set; }
        public virtual Student Student { get; set; }
        public DateTime AppliedDate { get; set; }
    }

    public class JobSortListed
    {
        public Guid JobId { get; set; }
        public virtual Job Job { get; set; }

        public int StudentId { get; set; }
        public virtual Student Student { get; set; }
        public bool IsSortListed { get; set; }
        public DateTime SortListedDate { get; set; }
        public bool IsEmailSent { get; set; }
    }
}
