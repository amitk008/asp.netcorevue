﻿namespace JEULPlacementSystem.Domain.Constant
{
    public class ApplicationConstants
    {
     
        public const string SuperAdminUserGuid = "1c3defe6-8080-7598-ab7a-62ecfde58a11";

        public const string SuperAdminRoleGuid = "1c3defe6-8080-7598-ab7a-62ecfde58a12";
        public const string UniversityRoleGuid = "1c3defe6-8080-7598-ab7a-62ecfde58a21";
        public const string CompanyRoleGuid = "1c3defe6-8080-7598-ab7a-62ecfde58a31";
        public const string StudentRoleGuid = "1c3defe6-8080-7598-ab7a-62ecfde58a41";
    }
}
