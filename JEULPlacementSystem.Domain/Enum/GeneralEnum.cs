﻿using System.ComponentModel;

namespace JEULPlacementSystem.Domain.Enum
{


    public enum UserType
    {

        [Description("Super Admin")] 
        SuperAdmin = 1,
        [Description("University")]
        University = 2,
        [Description("Company")]
        Company = 3,
        [Description("Student")]
        Student = 4,
    }

    public enum StudentRegistrationStatus
    {
        [Description("Pending")]
        Pending = 1,
        [Description("Approved")]
        Approved = 2
    }

    public enum GradingType
    {
        [Description("CGPA")]
        CGPA = 1,
        [Description("Percentage")]
        Percentage = 2
    }

    public enum JobType
    {
        [Description("Entry Level")]
        EntryLevel = 1,
        [Description("Mid Level")]
        MidLevel = 2,
        [Description("Senior Level")]
        SeniorLevel = 3
    }

    public enum MartialStatus
    {
        [Description("Un Married")]
        UnMarried = 1,
        [Description("Married")]
        Married = 2,
       
    }

    public enum ProfileAction
    {
        Default,
        Education,
        Experience 
    }
}
