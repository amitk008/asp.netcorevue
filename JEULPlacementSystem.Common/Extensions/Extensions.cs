﻿using System;
using System.IO;
using System.Data;
using Microsoft.AspNetCore.Http;
using JEULPlacementSystem.Model.Common;
using JEULPlacementSystem.Common.ViewModel.Common;




namespace JEULPlacementSystem.Common.Extensions
{
    public static class Extensions
    {
        public static UploadResultViewModel SaveFile(IFormFile file, string rootPath, int maxFileSize = 10)
        {

            var extension = Path.GetExtension(file.FileName);
            var folderName = Path.Combine("Resources", "CsvFile");
            var pathToSave = Path.Combine(rootPath, folderName);


            if (file.Length > 0)
            {

                var fullPath = Path.Combine(pathToSave, file.FileName);
                var dbPath = Path.Combine(folderName, file.FileName);
                string fileName = Path.GetFileNameWithoutExtension(dbPath);
                string fileExtension = Path.GetExtension(file.FileName);
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                return new UploadResultViewModel()
                {
                    Name = fileName,
                    NameWithExtension = fileExtension,
                    Extension = extension,
                    Path = fullPath,
                    Success = true,
                    PhysicalPath = fullPath
                };
            }
            else
                throw new ArgumentNullException("Error", innerException: null);

        }

        public static DataTable ConvertCSVtoDataTable(string strFilePath, bool csvContainsHeader = true)
        {
            DataTable dt = new DataTable();
            using (StreamReader sr = new StreamReader(strFilePath))
            {
                string[] headers = sr.ReadLine().Split(',');
                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(',');
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i].Trim();
                    }
                    dt.Rows.Add(dr);
                }

            }


            return dt;
        }

 
    }
}