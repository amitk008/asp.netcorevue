﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace JEULPlacementSystem.Common.Extensions
{
    public static class DatatableObjectComparer<T> where T : class
    {

        public static bool ValidateDatatable(DataTable datatable)
        {
            try
            {
                if (datatable == null)
                    return false;
                List<string> sourceFields = new List<string>();
                var typeinfo = typeof(T);
                var properties = typeinfo.GetProperties().Where(x => x.GetCustomAttributes(typeof(DataTableAttribute), false) != null).ToList();
                foreach (var item in properties)
                {
                    var incluedField = item.GetCustomAttributes(typeof(DataTableAttribute), false);
                    if (incluedField != null && incluedField.Count() > 0)
                        sourceFields.Add(item.Name);
                }
                if (!sourceFields.Any())
                    return false;
                List<string> dataTableFields = new List<string>();


                var columns = datatable.Columns;
                foreach (var item in columns)
                {
                    dataTableFields.Add(item.ToString().Replace("/r", "").TrimEnd());
                }
                if (!dataTableFields.Any())
                    return false;
                sourceFields = sourceFields.OrderByDescending(x => x).ToList();
                dataTableFields = dataTableFields.OrderByDescending(x => x).ToList();
                return CompareLists2(sourceFields, dataTableFields);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private static bool CompareLists2(List<string> list1, List<string> list2)
        {
            if (list1.Count != list2.Count)
                return false;

            for (int i = 0; i < list1.Count; i++)
            {
                if (list1[i].ToLower() != list2[i].ToLower())
                    return false;
            }

            return true;
        }
    }

    public class DataTableAttribute : Attribute
    {

    }
}
