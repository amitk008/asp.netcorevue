﻿using AutoMapper;
using JEULPlacementSystem.Common.Extensions;
using JEULPlacementSystem.Domain;
using JEULPlacementSystem.Domain.Enum;
using System;

namespace JEULPlacementSystem.Common.ViewModel
{

    public class StudentOnBoardingViewModel
    {
        [DataTable]
        public string Email { get; set; }
        public string Status { get; set; }
    }

    public class StudentCommonViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }

        public string Email { get; set; }
        public string PhoneNumber { get; set; }
       
    }

    public class StudentListViewModel: StudentCommonViewModel
    {
        public int Id { get; set; }
        public bool IsSortListed { get; set; }
        public bool IsEmailSent { get; set; }

    }

    public class StudentViewModel: StudentCommonViewModel
    {
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

    }

    public class StudentProfileViewModel
    {
        public int StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string MobileNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public MartialStatus MartialStatusId { get; set; }
        public string Nationality { get; set; }
        public JobType JobType { get; set; }
        public int JobTypeId
        {
            get
            {
                return (int)JobType;
            }
        }
        public string CurrentSalary { get; set; }
        public string ExpectSalary { get; set; }

        public string Skills { get; set; }
        public string Objective { get; set; }

        public string PermanentAddress { get; set; }
        public string CurrentAddress { get; set; }

    }

    public class StudentEducationDetailViewModel
    {
        public int Id { get; set; }
        public int StudentId { get; set; } 
        public string Degree { get; set; }
        public string Faculty { get; set; }
        public string Institute { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
       
        public int GradingTypeId { get; set; }
        public string GradingType
        {
            get
            {
                return Enum.GetName(typeof(GradingType), GradingTypeId);
            }
        }

        public double Marks { get; set; }

    }

    public class StudentExperienceDetailViewModel
    {
        public int Id { get; set; }
        public int StudentId { get; set; } 
        public string CompanyName { get; set; }
        public string JobTitle { get; set; }
        public int JobTypeId { get; set; }
         
        public string JobType
        {
            get
            {
                return Enum.GetName(typeof(JobType), JobTypeId);
            }
        }
        public bool IsCurrentJob { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Responsibilities { get; set; }

    }

    public class StudentMapper : Profile
    {
        public StudentMapper()
        {
      

            CreateMap<Student, StudentListViewModel>()
                .ForMember(src => src.FirstName, opts => opts.MapFrom(o => o.User.FirstName))
                .ForMember(src => src.LastName, opts => opts.MapFrom(o => o.User.LastName))
                .ForMember(src => src.Email, opts => opts.MapFrom(o => o.User.Email))
                .ForMember(src => src.PhoneNumber, opts => opts.MapFrom(o => o.User.PhoneNumber));

            CreateMap<Student, StudentViewModel>()
                .ForMember(src => src.FirstName, opts => opts.MapFrom(o => o.User.FirstName))
                .ForMember(src => src.LastName, opts => opts.MapFrom(o => o.User.LastName))
                .ForMember(src => src.Email, opts => opts.MapFrom(o => o.User.Email))
                .ForMember(src => src.PhoneNumber, opts => opts.MapFrom(o => o.User.PhoneNumber))
                .ForMember(src => src.FullName, opts => opts.MapFrom(o => o.User.FullName));
                

            CreateMap<StudentViewModel, User>()
                .ForMember(src => src.FullName, opts => opts.MapFrom(o => string.Join(" ", o.FirstName, o.LastName)))
                .ForMember(src => src.NormalizedEmail, opts => opts.MapFrom(o => o.Email.ToUpper()))
                .ForMember(src => src.NormalizedUserName, opts => opts.MapFrom(o => o.Email.ToUpper()));

            CreateMap<Student, MyProfileViewModel>();

            CreateMap<StudentProfile, StudentProfileViewModel>()
                 .ForMember(src => src.FirstName, opts => opts.MapFrom(o => o.Student.User.FirstName))
                 .ForMember(src => src.LastName, opts => opts.MapFrom(o => o.Student.User.LastName));
            CreateMap<StudentEducationDetail, StudentEducationDetailViewModel>();
            CreateMap<StudentExperienceDetail, StudentExperienceDetailViewModel>();
        }
    }

}
