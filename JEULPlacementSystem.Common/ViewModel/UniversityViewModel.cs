﻿using AutoMapper;
using JEULPlacementSystem.Domain;
using System;
using System.Linq;

namespace JEULPlacementSystem.Common.ViewModel
{
    public class UniversityCommonViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }

        public string PrimaryContactName { get; set; }
        public string PrimaryContactEmail { get; set; }
        public bool IsActive { get; set; }
        public bool IsApproved { get; set; }
    }

    public class UniversityViewModel : UniversityCommonViewModel
    {
      
    }

    public class UniversityListViewModel: UniversityCommonViewModel
    {
       
    }



    public class UniversityMapper : Profile
    {
        public UniversityMapper()
        {
            CreateMap<UniversityViewModel, University>();
            CreateMap<University, UniversityViewModel>()
                .ForMember(dest => dest.PrimaryContactName, src => src.MapFrom(x => x.UniversityUsers.FirstOrDefault().User.FullName))
                .ForMember(dest => dest.PrimaryContactEmail, src => src.MapFrom(x => x.UniversityUsers.FirstOrDefault().User.Email));

            CreateMap<University, UniversityListViewModel>()
               .ForMember(dest => dest.PrimaryContactName, src => src.MapFrom(x => x.UniversityUsers.FirstOrDefault().User.FullName));
        }
    }
}
