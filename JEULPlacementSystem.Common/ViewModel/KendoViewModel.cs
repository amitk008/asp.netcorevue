﻿using Kendo.DynamicLinqCore;
using System;
using System.Collections.Generic;

namespace JEULPlacementSystem.Common.ViewModel
{
    public class KendoDataRequest
	{
		public KendoDataRequest()
		{
			ExternalFilter = new Dictionary<string, object>();
		}
		public int Take { get; set; }
		public int Skip { get; set; }
		public IEnumerable<Sort> Sort { get; set; }
		public Filter Filter { get; set; }
		public IList<DataExtension> DataExtensions { get; set; }
		public Dictionary<string, object> ExternalFilter { get; set; }
		public bool IsReportPrint { get; set; }
	}
	public class DataExtension
	{
		public string Field { get; set; }
		public Type FieldType { get; set; }
		public string ActualField { get; set; }
		public string CustomType { get; set; }
		public object Columns { get; set; }
		public bool Ignore { get; set; }
	}
	public class DataSourceResult<T> where T : class
	{
		public object Aggregates { get; set; }
		public IEnumerable<T> Data { get; set; }
		public int Total { get; set; }
	}
}
