﻿using AutoMapper;
using JEULPlacementSystem.Domain;
using System;

namespace JEULPlacementSystem.Common.ViewModel
{

    public class DegreeCommonViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid FacultyId { get; set; }
    }

    public class DegreeViewModel : DegreeCommonViewModel
    {

    }

    public class DegreeListViewModel: DegreeCommonViewModel
    {

        public string FacultyName { get; set; }
    }

    public class DegreeMapper : Profile
    {
        public DegreeMapper()
        {
            CreateMap<DegreeViewModel, Degree>();
            CreateMap<Degree, DegreeViewModel>();
            CreateMap<Degree, DegreeListViewModel>()
                .ForMember(dest => dest.FacultyName, opt => opt.MapFrom(src => src.Faculty.Name));
        }
    }
}
