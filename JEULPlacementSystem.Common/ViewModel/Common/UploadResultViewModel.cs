﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JEULPlacementSystem.Common.ViewModel.Common
{
    public class UploadResultViewModel
    {
        public string Name { get; set; }
        public string Extension { get; set; }
        public string NameWithExtension { get; set; }
        public string Path { get; set; }
        public string PhysicalPath { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
