﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace JEULPlacementSystem.Model.Common
{
    public class Recurrent
    {
        public Recurrent()
        {
            Id = Guid.NewGuid().ToString();
            IsDeleted = false;
        }
        [Key]
        public string Id { get; set; }
        [JsonIgnore]
        public bool IsDeleted { get; set; }
        [JsonIgnore]
        public DateTime CreatedAt { get; set; }
        [JsonIgnore]
        public DateTime? DeletedAt { get; set; }
    }
}
