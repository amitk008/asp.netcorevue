﻿using JEULPlacementSystem.Common;
using JEULPlacementSystem.Domain.Enum;
using System;
using System.Collections.Generic;

namespace JEULPlacementSystem.Common.ViewModel
{
    public class SignInViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class SignInResponseViewModel
    {
       
    
        public string AccessToken { get; set; }
        public int ExpiresIn { get; set; }

        public LoginInfo LoginInfo { get; set; }
        public List<UserPageViewModel> UserPages { get; set; }
    }


    public class ResetPasswordViewModel
    {
        public Guid UserId { get; set; }
        public string Code { get; set; }
        public string Password { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        public string Email { get; set; }
    }


    public class EmailNotificationViewModel
    {
        public string LogoUrl { get; set; }
        public string EmailIconUrl { get; set; }
        public string WebUrl { get; set; }

    }
    public class ForgotPasswordNotificationViewModel : EmailNotificationViewModel
    {
        public string FullName { get; set; }
        public string CallbackUrl { get; set; }
    }

    public class MyProfileViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }

        public string PrimaryContactName { get; set; }
        public string PrimaryContactEmail { get; set; }

        public ProfileAction Action { get; set; }

        //Student Profile
        public StudentProfileViewModel StudentProfile { get; set; } = new StudentProfileViewModel();

        //Education 
        public List<StudentEducationDetailViewModel> StudentEducationDetails { get; set; } = new List<StudentEducationDetailViewModel>();
        public List<StudentExperienceDetailViewModel> StudentExperienceDetails { get; set; } = new List<StudentExperienceDetailViewModel>();
    }

}
