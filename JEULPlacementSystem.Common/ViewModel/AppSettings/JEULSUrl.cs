﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JEULPlacementSystem.Common.ViewModel.AppSettings
{
    public class JEULSUrl
    {
        public string APIUrl { get; set; }
        public string WebUrl { get; set; }
    }
}
