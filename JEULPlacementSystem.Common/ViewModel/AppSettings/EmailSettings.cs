﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JEULPlacementSystem.Common.ViewModel.AppSettings
{
    public class EmailConfiguration
    {

        public string ApiKey { get; set; }
        public string FromEmail { get; set; }
        public string FromName { get; set; }

    }
}
