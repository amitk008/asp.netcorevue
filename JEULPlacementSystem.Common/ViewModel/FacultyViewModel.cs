﻿using AutoMapper;
using JEULPlacementSystem.Domain;
using System;

namespace JEULPlacementSystem.Common.ViewModel
{
    public class FacultyViewModel 
    {
            public Guid Id { get; set; }
            public string Name { get; set; }
    }

    public class FacultyMapper : Profile
    {
        public FacultyMapper()
        {
            CreateMap<FacultyViewModel, Faculty>();
            CreateMap<Faculty, FacultyViewModel>();
        }
    }
}
