﻿using System;

namespace JEULPlacementSystem.Common
{
    public class LoginInfo
    {
        public Guid UserId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string DisplayPicture { get; set; }
        public string Role { get; set; }
        public Guid TenantId { get; set; } = Guid.Empty;
    }
}