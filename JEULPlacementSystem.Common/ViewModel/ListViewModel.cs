﻿using AutoMapper;
using JEULPlacementSystem.Domain; 

namespace JEULPlacementSystem.Common.ViewModel
{
    public class ListViewModel
    {
        public object Id { get; set; }
        public string Text { get; set; }
    }


    public class ListMapper : Profile
    {
        public ListMapper()
        {
            CreateMap<Degree, ListViewModel>()
                .ForMember(dest => dest.Text, src => src.MapFrom(x => x.Name));

            CreateMap<Faculty, ListViewModel>()
              .ForMember(dest => dest.Text, src => src.MapFrom(x => x.Name));



        }
    }
}
