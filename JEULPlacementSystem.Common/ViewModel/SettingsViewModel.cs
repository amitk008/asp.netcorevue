﻿namespace JEULPlacementSystem.Common.ViewModel
{

    public class EmailTemplateViewModel
    {
        public string SelectedEmailContent { get; set; }
        public string NotSelectedEmailContent { get; set; }
    }
 

}
