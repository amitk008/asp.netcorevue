﻿using AutoMapper;
using JEULPlacementSystem.Domain;
using System;

namespace JEULPlacementSystem.Common.ViewModel
{
    public class PlacementViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ExpireDate { get; set; }
    }

    public class PlacementMapper : Profile
    {
        public PlacementMapper()
        {
            CreateMap<PlacementViewModel, Placement>();
            CreateMap<Placement, PlacementViewModel>();
        }
    }
}
