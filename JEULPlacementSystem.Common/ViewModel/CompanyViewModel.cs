﻿using AutoMapper;
using JEULPlacementSystem.Domain;
using System;
using System.Linq;

namespace JEULPlacementSystem.Common.ViewModel
{
    public class CompanyViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }

        public string PrimaryContactName { get; set; }
        public string PrimaryContactEmail { get; set; }
    }


    public class CompanyMapper : Profile
    {
        public CompanyMapper()
        {
            CreateMap<CompanyViewModel, Company>();
            CreateMap<Company, CompanyViewModel>()
                .ForMember(dest => dest.PrimaryContactName, src => src.MapFrom(x => x.CompanyUsers.FirstOrDefault().User.FullName))
                .ForMember(dest => dest.PrimaryContactEmail, src => src.MapFrom(x => x.CompanyUsers.FirstOrDefault().User.Email));
        }
    }
}
