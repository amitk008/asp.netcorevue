﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JEULPlacementSystem.Common.ViewModel
{
    public class UserPageViewModel
    {
        public int PageId { get; set; }
        public string to { get; set; } //pagepath
        public string name { get; set; }
        public string _name { get; set; }

        public int PageDisplayOrder { get; set; }
        public int? PageParentPageId { get; set; }
        public List<UserPageViewModel> items { get; set; }
        
        public string icon { get; set; }
       
    }
}
