﻿using AutoMapper;
using JEULPlacementSystem.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JEULPlacementSystem.Common.ViewModel
{
    public class JobViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public DateTime DatePublished { get; set; }
        public DateTime EndDate { get; set; }
        public string Salary { get; set; }
        public string Requirements { get; set; }
        public Guid DegreeId { get; set; }
        public int EligibilityAge { get; set; }
        public string EligibilityNationality { get; set; }
        public int NoOfVacancy { get; set; }
        public string JobDescription { get; set; }

        public string JobType { get; set; }

        public int ViewCount { get; set; }
        public int ApplicantCount { get; set; }

        public CompanyViewModel Company { get; set; }
    }



    public class StudentJobListViewModel : JobViewModel
    {

        public bool IsApplied { get; set; }
        public bool IsSaved { get; set; }

    }

    public class JobCounterViewModel
    {
        public Guid Id { get; set; }
        public int StudentId { get; set; }

    }

    public class JobSortStudentListViewModel
    {
        public int Id { get; set; } // studentId
        public bool IsSortListed { get; set; }

        public bool IsEmailSent { get; set; }

    }

    public class JobSortListViewModel
    {
        public List<JobSortStudentListViewModel> StudentList { get; set; } = new List<JobSortStudentListViewModel>();
        public bool SendEmail { get; set; }
    }


    public class StudentSortListedNotificationViewModel
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Body { get; set; }

    }

    public class JobMapper : Profile
    {
        public JobMapper()
        {
            CreateMap<JobViewModel, Job>();
            CreateMap<Job, JobViewModel>()
                    .ForMember(dest => dest.ViewCount, src => src.MapFrom(x => x.JobViewCounts.Count()))
                    .ForMember(dest => dest.ApplicantCount, src => src.MapFrom(x => x.JobViewCounts.Count()));
            CreateMap<Job, StudentJobListViewModel>()
                .ForMember(dest => dest.ViewCount, src => src.MapFrom(x => x.JobViewCounts.Count()));
        }
    }
}
