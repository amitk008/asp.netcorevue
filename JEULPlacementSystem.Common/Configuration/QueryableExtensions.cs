﻿ 
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions; 
using JEULPlacementSystem.Common.ViewModel;
using AutoMapper;
 
using Kendo.DynamicLinqCore;

namespace JEULPlacementSystem.Common.Configuration
{
	public static class QueryableExtensions
	{
		public static DataSourceResult ToDataSourceResult<T>(this IQueryable<T> queryable, KendoDataRequest kendoDataRequest, Sort initialsort)
		{
			// Filter the data first
			queryable = Filter(queryable, kendoDataRequest.Filter, kendoDataRequest.DataExtensions);

			// Calculate the total number of records (needed for paging)
			var total = queryable.Count();

			// Sort the data
			queryable = Sort(queryable, kendoDataRequest.Sort, initialsort, kendoDataRequest.DataExtensions);

			// Finally page the data
			queryable = Page(queryable, kendoDataRequest.Take, kendoDataRequest.Skip);

			return new DataSourceResult
			{
				Data = queryable.ToList(),
				Total = total
			};
		}
		public static DataSourceResult ToDataSourceResult<T, TD>(this IQueryable<T> queryable, IMapper mapper, KendoDataRequest kendoDataRequest, Sort initialsort)
		{
			queryable = Filter(queryable, kendoDataRequest.Filter, kendoDataRequest.DataExtensions);

			// Calculate the total number of records (needed for paging)
			var total = queryable.Count();

			// Sort the data
			queryable = Sort(queryable, kendoDataRequest.Sort, initialsort, kendoDataRequest.DataExtensions);

			// Finally page the data
			queryable = Page(queryable, kendoDataRequest.Take, kendoDataRequest.Skip);
			var mapped = mapper.Map<IList<T>, IList<TD>>(queryable.ToList());
			return new DataSourceResult
			{
				Data = mapped,
				Total = total
			};
		}
        public static object GetFilterValue(this Filter filter, string key)
		{
			if (filter != null && filter.Filters.Any())
			{
				return filter.Filters.SingleOrDefault(c => c.Field == key)?.Value;
			}
			return null;
		}
		private static IQueryable<T> Filter<T>(IQueryable<T> queryable, Filter filter, IList<DataExtension> dataExtensions)
		{
			if (filter == null || filter.Logic == null)
				return queryable;


			if (dataExtensions != null && filter.Filters != null)
			{
				if (dataExtensions.Any(p => p.CustomType == "MCS"))
				{
					dataExtensions.Where(p => p.CustomType == "MCS").ToList().ForEach(oim =>
					{
						var str = filter.Filters.SingleOrDefault(c => c.Field == oim.Field);
						if (str == null) return;
						queryable = queryable.MultiValueContainsAnyAll(new Collection<string>() { str.Value.ToString() },
							false, (Expression<Func<T, string[]>>)oim.Columns, str.Operator);

						filter.Filters = filter.Filters.Except(new List<Filter>() { str });
						if (filter.Filters.Any() == false)
							return;
					});

					if (filter.Filters.Any() == false)
						return queryable;

				}
			}


			// Collect a flat list of all filters
			var filters = filter.All();
			var ignorestring = "Ignore-Filter";
			if (dataExtensions != null)
			{
				filters.ToList().ForEach(c =>
				{
					var dataExtension = dataExtensions.SingleOrDefault(p => p.Field == c.Field);
					if (dataExtension == null) return;
					if (dataExtension.Ignore)
						c.Field = ignorestring;
					if (!string.IsNullOrEmpty(dataExtension.ActualField))
						c.Field = dataExtension.ActualField;

					if (dataExtension.FieldType != null)
						c.Value = dataExtension.FieldType.IsEnum ? Enum.Parse(dataExtension.FieldType, c.Value.ToString()) : Convert.ChangeType(c.Value, dataExtension.FieldType);

				});
			}
			filters = filters.Where(c => c.Field != ignorestring).ToList();
			filter.Filters = filter.Filters.Where(c => c.Field != ignorestring).ToList();

			if (!filter.Filters.Any())
				return queryable;
			// Get all filter values as array (needed by the Where method of Dynamic Linq)
			var values = filters.Select(f => f.Value).ToArray();

			// Create a predicate expression e.g. Field1 = @0 And Field2 > @1
			string predicate = filter.ToExpression(typeof(T), filters);

			// Use the Where method of Dynamic Linq to filter the data
			queryable = queryable.Where(predicate, values);

			return queryable;
		}
		private static IQueryable<T> Sort<T>(IQueryable<T> queryable, IEnumerable<Sort> sort, Sort initialsort, IList<DataExtension> dataExtensions)
		{
			if (sort == null || !sort.Any())
			{
				sort = new List<Sort>() { initialsort };
			}


			if (dataExtensions != null && dataExtensions.Any(p => p.CustomType == "MCS"))
			{
				dataExtensions.Where(p => p.CustomType == "MCS").ToList().ForEach(oim =>
				{
					var s = sort.SingleOrDefault(c => c.Field == oim.Field);

					if (s == null)
						return;

					var direction = s.Dir;


					var e =
						((Expression<Func<T, string[]>>)oim.Columns).Body.ToString()
							.Replace("x.", "")
							.Replace("new [] {", "")
							.Replace("}", "")
							.Split(',');

					sort = sort.Except(new List<Sort>() { s });
					var newList = e.Select(param => new Sort()
					{
						Field = param,
						Dir = direction
					}).ToList();

					sort = sort.Concat(newList);

				});
			}

			if (dataExtensions != null)
			{
				sort.ToList().ForEach(c =>
				{
					var dataExtension = dataExtensions.SingleOrDefault(p => p.Field == c.Field);
					if (dataExtension == null) return;
					if (!string.IsNullOrEmpty(dataExtension.ActualField))
						c.Field = dataExtension.ActualField;
				});
			}
			//if (sort != null && sort.Any())
			//{
			// Create ordering expression e.g. Field1 asc, Field2 desc
			var ordering = String.Join(",", sort.Select(s => s.ToExpression()));

			// Use the OrderBy method of Dynamic Linq to sort the data
			return queryable.OrderBy(ordering);
			//}

			//return queryable;
		}
		private static IQueryable<T> Page<T>(IQueryable<T> queryable, int take, int skip)
		{
			return take == 0 ? queryable.Skip(skip) : queryable.Skip(skip).Take(take);
		}

		public static IQueryable<T> MultiValueContainsAnyAll<T>(this IQueryable<T> source, ICollection<string> searchKeys, bool all, Expression<Func<T, string[]>> fieldSelectors, string opt)
		{

			var containsMethod = typeof(string).GetMethod("Contains");
			var startsWithMethod = typeof(string).GetMethod("StartsWith", new Type[] { typeof(string) });
			var endsWithMethod = typeof(string).GetMethod("EndsWith", new Type[] { typeof(string) });


			if (source == null)
				throw new ArgumentNullException("source");
			if (fieldSelectors == null)
				throw new ArgumentNullException("fieldSelectors");
			var newArray = fieldSelectors.Body as NewArrayExpression;
			if (newArray == null)
				throw new ArgumentOutOfRangeException("fieldSelectors", fieldSelectors, "You need to use fieldSelectors similar to 'x => new string [] { x.LastName, x.FirstName, x.NickName }'; other forms not handled.");
			if (newArray.Expressions.Count == 0)
				throw new ArgumentException("No field selected.");
			if (searchKeys == null || searchKeys.Count == 0)
				return source;

			//MethodInfo containsMethod = typeof(string).GetMethod("Contains", new Type[] { typeof(string) });
			Expression expression = null;

			foreach (var searchKeyPart in searchKeys)
			{
				var tmp = new Tuple<string>(searchKeyPart);
				Expression searchKeyExpression = Expression.Property(Expression.Constant(tmp), tmp.GetType().GetProperty("Item1"));

				Expression oneValueExpression = null;
				foreach (var fieldSelector in newArray.Expressions)
				{
					Expression act = null;// = Expression.Call(, containsMethod, );
					switch (opt)
					{
						case "eq":
							act = Expression.Equal(fieldSelector, searchKeyExpression);
							break;
						case "neq":
							act = Expression.NotEqual(fieldSelector, searchKeyExpression);
							break;
						case "contains":
							act = Expression.Call(fieldSelector, containsMethod, searchKeyExpression);
							break;
						case "startswith":
							act = Expression.Call(fieldSelector, startsWithMethod, searchKeyExpression);
							break;
						case "endswith":
							act = Expression.Call(fieldSelector, endsWithMethod, searchKeyExpression);
							break;
					}

					oneValueExpression = oneValueExpression == null ? act : Expression.OrElse(oneValueExpression, act);
				}

				if (expression == null)
					expression = oneValueExpression;
				else if (all)
					expression = Expression.AndAlso(expression, oneValueExpression);
				else
					expression = Expression.OrElse(expression, oneValueExpression);
			}
			return source.Where(Expression.Lambda<Func<T, bool>>(expression, fieldSelectors.Parameters));
		}
	}
}
