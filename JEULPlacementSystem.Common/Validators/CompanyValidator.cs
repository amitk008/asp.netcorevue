﻿using FluentValidation;
using JEULPlacementSystem.Common.ViewModel;

namespace JEULPlacementSystem.Common.Validators
{

    public class CompanyValidator : AbstractValidator<CompanyViewModel>
    {
        public CompanyValidator()
        {
            RuleFor(x => x.Email).NotEmpty().EmailAddress().WithMessage("Email Address must be Valid!");
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Address).NotEmpty();
            RuleFor(x => x.Website).NotEmpty();
            RuleFor(x => x.PrimaryContactName).NotEmpty();
        }
    }
}
