﻿using FluentValidation;
using JEULPlacementSystem.Common.ViewModel;

namespace JEULPlacementSystem.Common.Validators
{
    public class SignInViewModelValidator : AbstractValidator<SignInViewModel>
    {
        public SignInViewModelValidator()
        {
            RuleFor(x => x.UserName).NotEmpty().EmailAddress().WithMessage("Email Address must be Valid!");
            RuleFor(x => x.Password).NotEmpty();
        }
    }
}
