﻿using FluentValidation;
using JEULPlacementSystem.Common.ViewModel;

namespace JEULPlacementSystem.Common.Validators
{

    public class StudentValidator : AbstractValidator<StudentViewModel>
    {
        public StudentValidator()
        {
            RuleFor(x => x.Email).NotEmpty().EmailAddress().WithMessage("Email Address must be Valid!");
            RuleFor(x => x.FirstName).NotEmpty();
            RuleFor(x => x.LastName).NotEmpty();
            RuleFor(x => x.PhoneNumber).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
            RuleFor(x => x.ConfirmPassword).NotEmpty().Equal(x=>x.ConfirmPassword)
                                                .WithMessage("Password and Confirm password does not match");
        }
    }
}
