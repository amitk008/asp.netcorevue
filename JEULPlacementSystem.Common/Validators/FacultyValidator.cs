﻿using FluentValidation;
using JEULPlacementSystem.Common.ViewModel;

namespace JEUL.Placement.Systemt.Common.Validators
{
    public class FacultyValidator : AbstractValidator<FacultyViewModel>
    {
        public FacultyValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
        }
    }
}
