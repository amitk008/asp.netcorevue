﻿using FluentValidation;
using JEULPlacementSystem.Common.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace JEULPlacementSystem.Common.Validators
{
    public class DegreeValidator : AbstractValidator<DegreeViewModel>
    {
        public DegreeValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.FacultyId).NotEmpty();
        }
    }
}
